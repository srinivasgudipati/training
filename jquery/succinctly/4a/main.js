// Topic: Wait for the DOM to be ready

$(document).ready(function () {
	$('p').text('DOM is now loaded and ready to be manipulated.');
});