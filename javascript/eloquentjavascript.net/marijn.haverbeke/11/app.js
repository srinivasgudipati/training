// Topic: Arrays

function range(start, end, step) {
	var array = [];
	step = step || (start < end ? 1 : -1);

	if (step > 0) {
		for (var i = start; i <= end; i = i + step ) {
			array.push(i);
		}
	} else {
		for (var i = start; i >= end; i = i + step ) {
			array.push(i);
		}
	}
	return array;
}

function sum(array) {
	var sum = 0;

	for (var i = 0; i < array.length; i++) {
		sum += array[i];
	}
	return sum;
}

console.log(sum(range(1, 10)));
console.log(sum(range(1, 10, 2)));
console.log(sum(range(5, 2, -2)));
console.log(sum(range(5, 2)))