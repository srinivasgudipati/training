import React, { Component } from 'react';
import { connect } from 'react-redux';

class CurrentCount extends Component {
	render() {
		return (
			<div className="current-count">
				<h2>
					Count: { this.props.currentCount }
				</h2>
			</div>
		);
	}
}

function mapStateToProps(state) {
	// Whatever is returned will show up as props inside of CurrentCount
	return {
		currentCount: state.currentCount
	}
}

export default connect(mapStateToProps)(CurrentCount);