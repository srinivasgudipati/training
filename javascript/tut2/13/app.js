// pass by value - primitives

var a = 2;
var b;

b = a;
a = 3;

console.log(a);
console.log(b);

// pass by reference - all objects (including functions)

var c = {greeting: 'hi'};
var d;

d = c;
c.greeting = 'hello';

console.log(c);
console.log(d);

// pass by reference - function parameters

function changeGreeting(obj) {
	obj.greeting = 'hola';
}

changeGreeting(c);

console.log(c);
console.log(d);

c = {greeting: 'howdy'};

console.log(c);
console.log(d);