// Topic: Recursive function

function factorial(n) {
  if (n < 0) {
    // Termination condition to prevent infinite recursion
    console.log('Not allowed');
    return;
  }

  // Base case
  if (n === 0) {
    return 1;
  }

  // Recursive case
  return n * factorial(n -1);
}

factorial(-1);
console.log(factorial(5));

/*
1.  factorial(5) {
      return 5 * factorial(4);
2.    factorial(4) {
        return 4 * factorial(3);
3.      factorial(3) {
          return 3 * factorial(2);
4.        factorial(2) {
            return 2 * factorial(1);
5.          factorial(1) {
              return 1 * factorial(0);
6.            factorial(0) {
                return 1;
              }
            }
          }
        }
      }
    }
*/