// Topic: Const Let

// ES5

// var name = 'Srinivas';
// var title = 'Software Engineer';
// var hourlyWage = 20;

// ES6

const name = 'Srinivas';
let title = 'Software Engineer';
let hourlyWage = 20;

// Some years later

title = 'Senior Software Engineer';
hourlyWage = 40;

console.log(title);
console.log(hourlyWage);