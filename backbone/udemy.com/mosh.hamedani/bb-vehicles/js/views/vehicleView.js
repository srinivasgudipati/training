define([
	'jquery',
	'underscore',
	'backbone',
	'models/vehicle'], function($, _, Backbone, Vehicle) {
		var VehicleView = Backbone.View.extend({
			tagName: "li",

			className: "vehicle",

			events: {
				"click .delete": "onClick"
			},

			initialize: function() {
				this.model.on("change", this.render, this);
			},

			onClick: function() {
				this.remove();
			},

			render: function() {
				var template = _.template($("#vehicleTemplate").html());

				this.$el.html(template(this.model.toJSON()));
				this.$el.attr("id", this.model.get("registrationNumber"));
				this.$el.attr("data-color", this.model.get("colour"));
				return this;
			}
		});

		return VehicleView;
});