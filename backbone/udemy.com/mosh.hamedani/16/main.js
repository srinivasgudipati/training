// Topic: Events: Binding and Triggering Custom Events

var person = {
	name: "Srinivas",
	walk: function() {
		// person object publishes custom event. first parameter is the event name, optional second parameter can be used to pass an object to subscriber
		this.trigger("walking", {
			startTime: "8:00 AM",
			speed: "10 mph"
		});
	}
};

_.extend(person, Backbone.Events); // Extend person object with capabilities defined in Backbone.Events

// person object subscribes to the above event. first parameter is the event name, second parameter is callback function, optional third one is for specifying the context
person.on("walking", function(e) {
	console.log("Person walking");
	console.log(e);
}, this);

person.walk();
person.off("walking"); // unsubscribe from the event
person.walk(); // no message

// 
person.once("walking", function(e) {
	console.log("Person walking again");
	console.log(e);
}, this);

person.walk();
person.walk(); // no message