define(["jquery", "methods"], function ($, methods) {
	$("#click").click(function () {
		methods.changeHTML("Hello there!");
	});
	
	$("#alert").click(function () {
		methods.showAlert("Hello again!");
	});
});