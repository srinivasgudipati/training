var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person(firstname, // can be accessed within this class and any class that extends this class
    lastname, // can be accessed by any one
    age // can be accessed only within this class
    ) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }
    Person.prototype.intro = function () {
        console.log("My name is " + this.firstname + " " + this.lastname + ". I am " + this.age + " years old");
    };
    return Person;
}());
var Programmer = /** @class */ (function (_super) {
    __extends(Programmer, _super);
    function Programmer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Programmer.prototype.intro = function () {
        console.log("My name is " + this.firstname + " " + this.lastname);
    };
    return Programmer;
}(Person));
var aPerson = new Person("Srinivas", "Gudipati", 43);
var aProgrammer = new Programmer("Sanjay", "Gudipati", 6);
aPerson.intro();
aProgrammer.intro();
