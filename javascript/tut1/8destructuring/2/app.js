// Topic: Destructuring: Objects

var saveFile = {
	extension: 'jpg',
	name: 'fire',
	size: '140 KB'
}

// ES5
function getSummary(saveFile) {
	return `${saveFile.name}.${saveFile.extension} is of size ${saveFile.size}`;
}

console.log(getSummary(saveFile));

// ES6

function getSummary1({ name, extension, size }) {
	return `${name}.${extension} is of size ${size}`;
}

console.log(getSummary1(saveFile));