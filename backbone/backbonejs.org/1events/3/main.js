// Topic: Events: "all" event

var person1 = {
	name: "Person 1",
	age: 40,
	changeName: function (name) {
		this.name = name;
		this.trigger("change:name", name);
	},
	changeAge: function (age) {
		this.age = age;
		this.trigger("change:age", age);
	}
};

var person2 = {
	name: "Person 2"
}

_.extend(person1, Backbone.Events);
_.extend(person2, Backbone.Events);

// Callbacks bound to the special "all" event will be triggered when any event occurs, and are passed the name of the event as the first argument.

person1.on("all", function(eventName) {
	person2.trigger(eventName);
});

person2.on("all", function(eventName) {
	console.log(eventName);
});

person1.on({
	"change:name": updateName,
	"change:age": updateAge
});

function updateName(name) {
	console.log("Name changed to ", name);
}

function updateAge(age) {
	console.log("Age changed to ", age);
}

person1.changeName("Person 1.1");
person1.changeAge(41);