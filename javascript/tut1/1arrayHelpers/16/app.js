// Topic: Array helpers: reduce

function unique(array) {
  return array.reduce(function(prev, curr) {
      if (!prev.find(function(num) {
      	return num === curr;
      })) prev.push(curr);
      return prev;
  }, []);
}

console.log(unique([1, 1, 2, 4, 3, 2, 4, 4]));