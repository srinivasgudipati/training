function asyncTask(fn) {
  return setTimeout(function(){ fn(); }, 2000);
}

function foo(n, fn) {
  asyncTask(function(val){ fn(n); });
}
function bar(n, fn) {
  asyncTask(function(val){ fn(n); });
}
function baz(n, fn) {
  asyncTask(function(val){ fn(n); });
}

var n = 1;
foo(n, function(res) {
  bar(res+1, function(res) {
    baz(res+1, function(res) {
      console.log("result =", res);
    });
  });
});