class C {
	constructor() {
		this.x = "x";
	}

	m1() {
		this.foo();
	}

	foo() {
		console.log(this);
	}
}

var c = new C();
c.m1();