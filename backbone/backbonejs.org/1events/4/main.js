// Topic: Events: space-delimited list of events

var book = {
	title: "Hello Brother",
	subtitle: "How are you",
	changeTitle: function (title) {
		this.title = title;
		this.trigger("change:title", title);
	},
	changeSubtitle: function (subtitle) {
		this.subtitle = subtitle;
		this.trigger("change:subtitle", subtitle);
	}
};

_.extend(book, Backbone.Events);

// Event string may also be a space-delimited list of several events
book.on("change:title change:subtitle", update);

function update(value) {
	console.log(value);
}

book.changeTitle("Hello Sister");
book.changeSubtitle("How are you?");