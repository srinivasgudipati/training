define([
	'backbone',
	'models/car',
	'views/carView'], function(Backbone, Car, CarView) {

	var CarsView = Backbone.View.extend({
		tagName: "ul",

		render: function() {
			var self = this;
			this.collection.each(function(Car) {
				var view = new CarView({ model: Car });
				self.$el.append(view.render().$el);
			});

			return this;
		}
	});

	return CarsView;
});