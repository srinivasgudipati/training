greet();

function greet() {
	console.log('hi');
}

var greetExpression = function () {
	console.log('hi');
};

greetExpression();

function log(a) {
	a();
}

log(greetExpression);

log(function () {
	console.log('hi');
});

log(greet);