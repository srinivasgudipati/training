// function statement
function greet() {
	console.log('Hi');
}
greet();

// first-class function
function logGreeting(fn) {
	fn();
}
logGreeting(greet);

// function expression
var greetMe = function() {
	console.log('Hi Srini!');
}
greetMe();

// first-class function
logGreeting(greetMe);

// on the fly function expression
logGreeting(function() {
	console.log('Hello Srini!');
});