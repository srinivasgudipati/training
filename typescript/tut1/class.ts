class Person {
  firstname: string;
  lastname: string;

  constructor(firstname: string, lastname: string) {
    this.firstname = firstname;
    this.lastname = lastname;
  }

  getFullName() {
    return this.firstname + ' ' + this.lastname;
  }
}

var aPerson = new Person("Srinivas", "Gudipati");

console.log(aPerson.getFullName());