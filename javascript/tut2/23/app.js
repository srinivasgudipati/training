function add(num1, num2) {
	return function(value1, value2) {
		return num1 + num2;
	};
}

var func = add(2, 5);
console.log(func());