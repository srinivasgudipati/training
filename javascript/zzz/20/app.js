// Topic: Bad inheritance

function Fee() {
	// ...
}

function Fi() {
	// ...
}

var fee = new Fee();

Fi.prototype = fee;

Fee.prototype.prop1 = 'prop1';
Fi.prototype.prop2 = 'prop2';

var fi = new Fi();

console.log(fee.prop1);
console.log(fi.prop2);

console.log(fee.prop2); // bad inheritance
console.log(fi.prop1);