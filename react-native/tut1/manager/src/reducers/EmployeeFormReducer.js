import {
	EMPLOYEE_CHANGED,
	EMPLOYEE_CREATE_SUCCESS,
	EMPLOYEE_UPDATE_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
	name: '',
	phone: '',
	shift: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case EMPLOYEE_CHANGED:
			return { ...state, [action.payload.prop]: action.payload.value };
		case EMPLOYEE_CREATE_SUCCESS:
			return INITIAL_STATE;
		case EMPLOYEE_UPDATE_SUCCESS:
			return INITIAL_STATE;
		default:
			return state;
	}
};
