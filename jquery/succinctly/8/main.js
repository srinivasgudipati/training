(function ($) {
	$("#list") // Original wrapper set
		.find("> li") // Destructive method - Returns <li>s which are direct descendants of #list
			.filter(":last") // Destructive method - Returns last <li> from the current list of <li>s
				.addClass("last")
			.end() // End .filter(":last")
			.find("ul") // Destructive method - Returns all <ul>s which are under <li>
				.css("background", "#ccc")
				.find("li:last") // Destructive method - Returns last <li> under <ul>
					.addClass("last")
				.end() // End .find("li:last")
			.end() // End .find("ul")
		.end() // End .find("> li") Back to the original $("#list")
		.find("li") // Destructive method - Returns all <li>s under #list
			.append("I am an &lt;li&gt;");
})(jQuery);