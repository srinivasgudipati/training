// Topic: Multifaceted aspects of jQuery

jQuery(function ($) { // Pass jQuery a function. A shortcut for ready() event
	$("<p></p>").appendTo("div#container"); // Pass jQuery a string of HTML
	$(document.createElement("a")).text("jQuery").appendTo("p"); // Pass jQuery an element reference.
	$("a:first").attr("href", "http://jquery.com"); // Pass jQuery a CSS expression.
});