// Topic: Block Helpers

var template = Handlebars.compile($("#the-template").html());

Handlebars.registerHelper("makeRadio", function(name, options) {
	var radioList = options.fn().trim().split('\n');
	var output = "";
	for (var i in radioList) {
		var item = radioList[i].trim();
		output += '<input type="radio" name="' + name + '" value="' + item + '">' + item + '<br>';
	}
	return output;
});
$("#data").html(template());