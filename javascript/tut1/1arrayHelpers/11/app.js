// Topic: Array helpers: every, some

var numbers = [
	2, 4, 6, 9, 10
];

var allEven1 = true,
	someEven1 = false,
	allEven2 = true,
	someEven2 = true;

// using for loop
for (var i = 0; i < numbers.length; i++) {
	if (numbers[i]%2 !== 0) {
		allEven1 = false;
	} else {
		someEven1 = true;
	}
}

console.log(allEven1);
console.log(someEven1);

// using every and some helper
allEven2 = numbers.every(function(number) {
	return number%2 === 0;
});

console.log(allEven2);

someEven2 = numbers.some(function(number) {
	return number%2 === 0;
});

console.log(someEven2);