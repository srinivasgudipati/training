// Topic: Default Function Arguments

// ES5
function makeAjaxRequest(url, method) {
	method = method || 'GET';
	return method;
}

console.log(makeAjaxRequest('google.com'));
console.log(makeAjaxRequest('google.com', null));
console.log(makeAjaxRequest('google.com', undefined));
console.log(makeAjaxRequest('google.com', 'POST'));

console.log('-----');

// ES6
function makeAjaxRequest1(url, method = 'GET') {
	return method;
}

console.log(makeAjaxRequest1('google.com'));
console.log(makeAjaxRequest1('google.com', null));
console.log(makeAjaxRequest1('google.com', undefined));
console.log(makeAjaxRequest1('google.com', 'POST'));