import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux';

import LibraryListItem from './LibraryListItem';

class LibraryList extends Component {
	componentWillMount() {
		console.log('LibraryList:componentWillMount');
		const ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2
		});

		this.dataSource = ds.cloneWithRows(this.props.libraries);
	}

	renderRow(library) {
		return <LibraryListItem library={library} />;
	}

	render() {
		console.log('LibraryList:render');
		return (
			<ListView
				dataSource={this.dataSource}
				renderRow={this.renderRow}
			/>
		);
	}
}

const mapStateProps = state => {
	console.log('LibraryList:mapStateProps');
	console.log('LibraryList:mapStateProps::state', state);
	return {
		libraries: state.libraries
	};
};

export default connect(mapStateProps)(LibraryList);
