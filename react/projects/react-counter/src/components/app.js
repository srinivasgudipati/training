import React, { Component } from 'react';

import CounterChanger from '../containers/counter_changer';
import CurrentCount from '../containers/current_count';

export default class App extends Component {
	render() {
		return (
			<div className="counter-box">
				<CurrentCount />
				<CounterChanger />
			</div>
		);
	}
}