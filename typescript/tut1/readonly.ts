class Person {
  readonly name = 'Srinivas'; // value can be set for a readonly attribute during declaration

  constructor(name) {
    this.name = name; // value can be set for a readonly attribute inside a constructor
  }
}

class Animal {
  constructor(readonly name: string) {
  }
}

var aPerson = new Person('Sanjay');
console.log(aPerson.name);

var aAnimal = new Animal('Lion');
console.log(aAnimal.name);