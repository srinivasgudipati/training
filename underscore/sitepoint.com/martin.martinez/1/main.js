// Topic: Objects: isEmpty

var str = "";
var arr = [];
var obj1 = null;
var obj2 = {};
var obj3;

console.log(_.isEmpty(str));
console.log(_.isEmpty(arr));
console.log(_.isEmpty(obj1));
console.log(_.isEmpty(obj2));
console.log(_.isEmpty(obj3));