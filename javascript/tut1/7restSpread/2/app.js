// Topic: Spread operator

const asianCountries = ['India', 'China', 'Japan'];

const europeanCountries = ['UK', 'Germany', 'Switzerland', 'Spain'];

const countries = ['Nigeria', ...asianCountries, ...europeanCountries];

console.log(countries);

console.log('Libya', ...countries);