// Topic: Arrays: each

var artists = ['Pharel Williams', 'Rolling Stones', 'Michael Jackson'];

_.each(artists, function (artist) {
	var el = document.createElement('p');
	el.innerHTML = artist;
	document.body.appendChild(el);
});