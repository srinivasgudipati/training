define(['backbone'], function(Backbone) {

	var CarView = Backbone.View.extend({
		tagName: "li",

		render: function() {
			this.$el.html(this.model.get("name"));

			return this;
		}
	});

	return CarView;
});