// Topic: Views

var SongView = Backbone.View.extend({
	render: function() {
		this.$el.html("Hello World");

		return this;
	}
});

var songView = new SongView(); 	/*
								If you dont specify DOM element while instantiating view,
								backbone automatically creates "div" element for the view.
								But this DOM element in the memory and it is not inserted in the HTML markup
								*/
$("#container").html(songView.render().$el);