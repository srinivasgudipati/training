var a: number; // number type declaration
var b: boolean; // boolean type declaration
var c: string; // string type declaration
var d: undefined; // better to assign undefined value to a variable instead of declaring it as undefined type as that is the only value it can hold
var e: null; // better to assign null value to a variable instead of declaring it as null type as that is the only value it can hold

a = 10;
b = true;
c = 'hello';

var arr1: number[]; // number array type declaration
arr1 = [1, 2];

var arr2: [number, string]; // tuple. data types are specified like elements inside []
arr2 = [1, 'hello'];

var x = 10; // Implicit typing. Only works when value is assigned during variable declaration

var y;
y = 10;
y = 'Hello';

var m: any;

m = 10;
m = 'Hello';

var n: string | number;

n = 'Hello';
n = 10;