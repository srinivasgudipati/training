// Topic: Wait for the DOM to be ready

// Shortcut with fail-safe usage of $
jQuery(function ($) {
	$('p').text('DOM is now loaded and ready to be manipulated.');
	// Use $() without fear of conflicts
});