// Topic: Collection: url

var Post = Backbone.Model.extend();

var Posts = Backbone.Collection.extend({
	url: 'http://jsonplaceholder.typicode.com/posts',
	model: Post
});

var posts = new Posts();

//posts.add({id: 100});
/*posts.first().fetch().then(function(post) {
	console.log(post);
});*/

posts.fetch({
	success: function(posts) {
		console.log(posts.first());
	}
});