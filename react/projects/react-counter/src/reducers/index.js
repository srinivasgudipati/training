import { combineReducers } from 'redux';
import CurrentCountReducer from './reducer_current_count';

const rootReducer = combineReducers({
	currentCount: CurrentCountReducer
});

export default rootReducer;