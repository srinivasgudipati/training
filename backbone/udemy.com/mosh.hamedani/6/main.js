// Topic: Collections

var Song = Backbone.Model.extend();

var Songs = Backbone.Collection.extend({
	model: Song
});

var songs = new Songs([
				new Song({ title: "Song 1", genre: 'Jazz', downloads: 100 }),
				new Song({ title: "Song 3", genre: 'Pop', downloads: 50 })
			]);

songs.push(new Song({ title: "Song 4", genre: 'Jazz', downloads: 200 })); // push adds this model to the top of the stack
songs.add(new Song({ title: "Song 2", genre: 'Jazz', downloads: 300 }), { at: 1 }); // add this model at index 1 of the array

console.log(songs.at(0)); // Song 1
console.log(songs.get("c1")); // Song 1

songs.remove(songs.at(0)); // Song 1 is removed
console.log(songs);

console.log(songs.where({ genre: 'Jazz' })); // Song 2 and Song 4 are returned
console.log(songs.findWhere({ genre: 'Jazz' })); // Song 2 is returned
console.log(songs.where({ genre: 'Jazz', downloads: 200 })); // Song 4 is returned
console.log(songs.filter(function(song) {
	return song.get("downloads") < 100;
})); // Song 3 is returned
console.log(songs.each(function(song) {
	return song;
})); // Song 2, Song 3 and Song 4 are returned
songs.pop(); // Song 4 is removed
console.log(songs); // Song 2 and Song 3