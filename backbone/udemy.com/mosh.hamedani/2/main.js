// Topic: Model Attributes

var Song = Backbone.Model.extend({
	defaults: {
		language: "English"
	}
});

// Method 1 - by passing two parameters to set method
var song1 = new Song();

song1.set("name", "A whole new world!");
song1.set("album", "Aladdin");

// Method 2 - by passing object to set method
var song2 = new Song();

song2.set({
	"name": "Work work work",
	"Artist": "Rhianna"
});

// Method 3 - while initializing model
var song3 = new Song({
	"name": "Remember the time",
	"Artist": "Michael Jackson"
});

console.log(song3.get("Artist"));
console.log(song3.has("Artist"));
song3.unset("name");
song3.clear();