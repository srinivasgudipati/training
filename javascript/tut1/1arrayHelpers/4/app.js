// Topic: Array helpers: map

var cars = [
	{ model: 'Camry', price: 'Cheap' },
	{ model: 'Lexus', price: 'Expensive' }
];

var prices = cars.map(function(car) {
	return car.price
});

console.log(prices);