// Topic: Enhanced Object Literals

function createBookShop(inventory) {
	return {
		inventory,
		inventoryValue() {
			return this.inventory.reduce((total, book) => total + book.price, 0);
		},
		priceForTitle(title) {
			return this.inventory.find(book => book.title === title);
		}
	}
}

const inventory = [
	{
		title: 'Harry Potter',
		price: 20
	},
	{
		title: 'Eloquent Javascript',
		price: 40
	}
];

const bookShop = createBookShop(inventory);

console.log(bookShop.inventory);
console.log(bookShop.inventoryValue());
console.log(bookShop.priceForTitle('Eloquent Javascript').price);