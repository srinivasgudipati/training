class Person {
  firstname: string;
  lastname: string;
  greet() {
    console.log('Hello there!');
  }
}

class Programmer extends Person { // inheritance
  greet() {
    console.log("Hello World!");
  }

  greet1() {
    super.greet(); // to refer to the public and protected methods of the inherited class
  }
}

var aProgrammer = new Programmer();
aProgrammer.greet();
aProgrammer.greet1();

var aProgrammer1: Person = new Programmer(); //polymorphism
aProgrammer1.greet(); //aProgrammer instance has acess only to the methods of Person class but executes the method defined in Programmer as it its instance