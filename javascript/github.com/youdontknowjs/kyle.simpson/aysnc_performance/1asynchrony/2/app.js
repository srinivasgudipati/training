var a = {
	index: 1
};

// Ideally the value of 'a' should be { index: 1 } at this point of execution. However console.log is an I/O operation which is asynchronous. So if this operation is defered to the background due to some reason, JS will continue with its execution and a is modified to { index: 2 }. Once I/O is avaiable, a will be printed with its modified value.
console.log(a);

a.index++;