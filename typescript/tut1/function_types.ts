// function arguments can be declared to be of particular type
// typescript expects the function to be called with the default arguments
// optional arguments should be at the end of the list of arguments
// return type of a function can be specified
function add(num1: number, num2: number, num3: number = 0, num4?: number): number {
  return num1 + num2 + num3;
}

console.log(add(1, 2, 3, 4));