/* 
	Topic: jQuery('span') will return a set of all the 'span' elements from the HTML page and wrap them with jQuery methods.
	This set is referred to as 'wrapper set'.
	When text() method is applied on the wrapper set, it is performed on each and every 'span' element in the wrapper set and returns combined string of text contained in all the 'span' elements within the wrapper set
*/
console.log(jQuery('span').text());