// Topic: Helpers

var template = Handlebars.compile('{{makeLink text url}}');

// Registers helpers accessible by any template in the env
Handlebars.registerHelper('makeLink', function (text, url) {
	// HTML escapes the passed string, making it safe for rendering as text within HTML content
	text = Handlebars.Utils.escapeExpression(text); 
	url = Handlebars.Utils.escapeExpression(url);
	var theLink = '<a href="'+url+'">'+text+'</a>';
	// Prevents theLink from being escaped when the template is rendered
	return new Handlebars.SafeString(theLink);
});

$("#data").html(template({
	text: "yahoo",
	url: "http://www.yahoo.com"
}));