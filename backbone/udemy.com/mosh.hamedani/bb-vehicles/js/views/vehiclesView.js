define([
	'jquery',
	'underscore',
	'backbone',
	'models/vehicle',
	'views/vehicleView'], function($, _, Backbone, Vehicle, VehicleView) {
		var VehiclesView = Backbone.View.extend({
			tagName: "ul",

			id: "vehicles",

			initialize: function(options) {
				this.bus = options.bus;
				// We pass "this" as the third argument so inside
				// onNewVehicle method, we can access it. If 
				// you don't set the "this" here, and you access
				// "this" inside onNewVehicle, it won't be pointing
				// to the view itself. This is how Javascript works.
				this.bus.on("newVehicle", this.onNewVehicle, this);
				this.collection.on("add", this.onNewVehicle, this);
				this.collection.on("remove", this.onRemove, this);
			},

			onNewVehicle: function(vehicle) {
				this.$el.append(new VehicleView({ model: vehicle }).render().$el);
			},

			onRemove: function(vehicle) {
				this.$el.find("li#" + vehicle.get("registrationNumber")).remove();
			},

			render: function() {
				var self = this;

				this.collection.each(function(vehicle) {
					var vehicleView = new VehicleView({ model: vehicle });
					self.$el.append(vehicleView.render().$el);
				});

				return this;
			}
		});

		return VehiclesView;
});