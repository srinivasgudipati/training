'use strict';

var Greetr = require('./greetr');

var greetr = new Greetr();
greetr.on('greet', function(data) {
	console.log('Someone said hello:', data);
});

greetr.greet('Srinivas');
