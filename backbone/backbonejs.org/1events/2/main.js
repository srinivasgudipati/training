// Topic: Events: Using map syntax

var book = {
	title: "Hairy Potter",
	
	author: "J.K.Rowlin",
	
	changeTitle: function(title) {
		this.title = title;
		// In case large number of events on a page, convention is to use colons to namespace events
		this.trigger("change:title", title);
	},

	changeAuthor: function(author) {
		this.author = author;
		this.trigger("change:author", author);
	},

};

_.extend(book, Backbone.Events);

// Backbone event methods support event map syntax as an alternative to positional arguments
book.on({
	"change:title": updateTitle,
	"change:author": updateAuthor
});

function updateTitle(title) {
	console.log("Title changed to ", title);
}

function updateAuthor(author) {
	console.log("Author changed to ", author);
}

book.changeTitle("Harry Potter");
book.changeAuthor("J.K.Rowling");