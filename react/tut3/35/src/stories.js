import React from 'react';

export function PhotoStory(props) {
	return (
		<div>
			<img src="https://www.google.com/logos/doodles/2015/googles-new-logo-5078286822539264.3-hp2x.gif" />;
			<p>
				{props.story}
			</p>
		</div>
	)
}

export function VideoStory(props) {
	return (
		<div>
			<div className="embed-responsive embed-responsive-16by9">
				<iframe className="embed-responsive-item" src="https://www.youtube.com/embed/lB95KLmpLR4"></iframe>
			</div>
			<p>
				{props.story}
			</p>
		</div>
	);
}