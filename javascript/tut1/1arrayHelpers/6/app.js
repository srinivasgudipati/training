// Topic: Array helpers: filter

var post = { id: 4, title: 'New Post' };
var comments = [
	{ postId: 4, comment: 'Love it' },
	{ postId: 5, comment: 'Hate it' },
	{ postId: 4, comment: 'Aweomse sauce!' },
	{ postId: 4, comment: 'Whats up' }
];

function commentsForPost(post, comments) {
	return comments.filter(function(comment) {
		return comment.postId === post.id		
	});
}

console.log(commentsForPost(post, comments));