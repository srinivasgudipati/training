define(["jquery"], function ($) {
	var methods = {};
	
	methods.changeHTML = function (arg) {
		$("div").html(arg);
	};
	
	methods.showAlert = function (arg) {
		alert(arg);
	}
	
	return methods;
});