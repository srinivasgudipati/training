// Create an empty array called "stack"
var stack = [];
// Here is our recursive function
function power(base, exponent) {
  // Base case 
  if ( exponent === 0 ) {
    return 1;
  }
  // Recursive case
  else {
	stack[exponent - 1] = base * power(base, exponent - 1);
    return stack[exponent - 1];
  }
}

console.log(power(3, 3));
console.log(stack);

/*
Control flow in a recursive function
1.	power(3, 3) {
		stack[2] = 3 * power(3, 2);
2.		power(3, 2) {
			stack[1] = 3 * power(3, 1);
3.			power(3, 1) {
				stack[0] = 3 * power(3, 0);
4.				power(3, 0) {
					return 1;
				}
				return stack[0]; // 3
			}
			return stack[1]; // 9
		}
		return stack[2]; // 27
	}
*/