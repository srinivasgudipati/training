// Topic: Destructuring: Arrays

const colors = [
	'red',
	'pink',
	'green'
]

const [ name1, name2, ...rest ] = colors;

console.log(name1);
console.log(name2);
console.log(rest);
