// Topic: Events: Off

var person = {
	name: "Srinivas",
	age: 41,
	changeName: function(name) {
		this.name = name;
		person.trigger("change:name", name);
	},
	changeAge: function(age) {
		this.age = age;
		person.trigger("change:age", age);
	}
};

_.extend(person, Backbone.Events);

person.on("change:name", onChange1);

person.on("change:name", onChange2);

person.on("change:age", onChange1);

person.on("change:age", onChange2);

function onChange1(value) {
	console.log("1: ", value);
}

function onChange2(value) {
	console.log("2: ", value);
}

// person.off("change:name", onChange1); // Removes just the `onChange1` callback

// person.off("change:name"); // Removes all `hange:name` callbacks

// person.off(null, onChange1); // Removes `onChange1` callback for all events

// person.off(); // Removes all callbacks for all events

person.changeName("Sanjay");

person.changeAge("5");