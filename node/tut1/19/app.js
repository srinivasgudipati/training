var obj = {
	name: 'John Doe',
	greet: function(param1, param2) {
		console.log(`Hello ${this.name}`);
		console.log(param1);
		console.log(param2);
	}
};

obj.greet('param1', 'param2');
obj.greet.call({ name: 'Jane Doe' }, 'param1', 'param2');
obj.greet.apply({ name: 'Jane Doe' }, ['param1', 'param2']);