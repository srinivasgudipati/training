var person = {
	firstname: "Default",
	lastname: "Default",
	getFullName: function() {
		return this.firstname + ' ' + this.lastname;
	}
};

var john = {
	firstname: "John",
	lastname: "Doe"
}

john.__proto__ = person;

var jane = {
	firstname: "Jane"
};

jane.__proto__ = john;

console.log(jane.getFullName());

for(var prop in jane) {
	if(jane.hasOwnProperty(prop))
		console.log(prop + ': ' + jane[prop]);
}