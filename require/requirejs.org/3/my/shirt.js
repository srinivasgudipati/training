// shirt.js now does setup work before returning its module definition.

define(["cart", "inventory"], function (cart, inventory) {

	return {
		color: "blue",
    size: "large",
		addToCart: function () {
			cart.add(this);
			//inventory.decrement(this);
		}
	}
});