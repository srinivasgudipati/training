// Topic: Objects: values

var foo = { a: 1, b: 2 };

var bar = Object.create(foo);

bar.c = 3;
bar.d = 4;

console.log(_.values(bar)); // ["3", "4"] - returns all the values of the object's own enumerable properties