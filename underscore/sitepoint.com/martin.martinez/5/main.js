// Topic: Utiltity: template

var artists = ['Pharel Williams', 'Rolling Stones', 'Michael Jackson'],
		artistTemplate = _.template('<li><%= artist %></li>'),
		content = '';

_.each(artists, function (artist) {
	content += artistTemplate({
		artist: artist
	});
});

var container = document.createElement('ol');
container.innerHTML = content;
document.body.appendChild(container);