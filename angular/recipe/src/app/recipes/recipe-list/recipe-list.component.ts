import { Component, OnInit } from '@angular/core';

import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('Casserole', 'Recipe Description', 'https://pixnio.com/free-images/food-and-drink/casserole-dish-filled-with-a-freshly-cooked-recipe-of-lemon-walnut-green-beans-which-based-on-its-ingredients-725x456.jpg'),
    new Recipe('Casserole', 'Recipe Description', 'https://pixnio.com/free-images/food-and-drink/casserole-dish-filled-with-a-freshly-cooked-recipe-of-lemon-walnut-green-beans-which-based-on-its-ingredients-725x456.jpg'),
  ];

  constructor() { }

  ngOnInit() {
  }

}
