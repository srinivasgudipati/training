// Topic: Array helpers: filter

var products = [ 
	{ name: 'Potato', type: 'vegetable' },
	{ name: 'Banana', type: 'fruit'},
	{ name: 'Carrot', type: 'vegetable' },
	{ name: 'Grape', type: 'fruit'}
];

var products1 = [],
	products2 = [],
	products3 = [],
	products4 = [];

// using for loop
for (var i = 0; i < products.length; i++) {
	if (products[i].type === 'fruit') {
		products1.push(products[i]);
	}
}

console.log(products1);

// using forEach helper
products.forEach(function(product) {
	if (product.type === 'fruit') {
		products2.push(product);
	}
});

console.log(products2);

// using filter helper
products3 = products.filter(function(product) {
	return product.type === 'fruit';
});

console.log(products3);