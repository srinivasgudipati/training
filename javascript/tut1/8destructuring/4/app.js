// Topic: Destructuring Arrays and Objects at the same time
const companies = [
	{name: 'Google', location: 'Mountain View'},
	{name: 'Facebook', location: 'Menlo Park'},
	{name: 'Uber', location: 'San Francisco'}
];

const [ { location } ] = companies;

console.log(location);

const company = {name: 'Google', locations: ['Monutain View', 'New York', 'Hyderabad']};

const { locations: [location1] } = company;

console.log(location1);