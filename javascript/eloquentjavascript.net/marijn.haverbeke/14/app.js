// Topic: Arrays

function deepEqual(val1, val2) {
	if (val1 === val2) return true;

	if (val1 == null || typeof val1 != "object" ||
			val2 == null || typeof val2 != "object")
			return false;

	var props1 = 0,
			props2 = 0;

	for (var prop in val1) {
		++props1;
	}

	for (var prop in val2) {
		++props2;
		if (!(prop in val1) || !deepEqual(val1[prop], val2[prop]))
			return false;
	}

	return props1 == props2;
}

console.log(deepEqual(1, 1));
// → true
console.log(deepEqual(1, "1"));
// → false
console.log(deepEqual(null, null));
// → true
console.log(deepEqual(null, 2));
// → false
console.log(deepEqual({ a: 1 }, 2));
// → false
console.log(deepEqual({ a: 1 }, { b: 1}));
// → false
console.log(deepEqual({ a: 1 }, { a: 1, b: 2}));
// → false
var obj = {here: {is: "an"}, object: 2};
console.log(deepEqual(obj, obj));
// → true
console.log(deepEqual(obj, {here: 1, object: 2}));
// → false
console.log(deepEqual(obj, {here: {is: "an"}, object: 2}));
// → true