// Topic: Rest, spread operators

function validate(...items) {
	if (items.indexOf('Milk') < 0) {
		return ['Milk', ...items];
	}

	return items;
}

console.log(validate('eggs', 'cereal'));