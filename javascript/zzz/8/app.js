// Topic: Create function using "Function"

function foo(x, y) {
	return x * y;
}

console.log(foo(10, 20));

var bar = new Function("x", "y", "return x*y;");

console.log(bar(10,20));