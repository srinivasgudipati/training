// Topic: Fat arrows

// using ES5
const c = {
	name: 'object name is c',
	log: function() {
		this.name = 'object c is modified!';
		console.log(this.name);
		const setName = function(name) {
			this.name = name;
		};
		setName('object c is modified again!');
		console.log(this.name);
	}
}

c.log();

console.log('--------');

// using ES5 - that
const c1 = {
	name: 'object name is c',
	log: function() {
		var that = this;
		this.name = 'object c is modified!';
		console.log(this.name);
		const setName = function(name) {
			that.name = name;
		};
		setName('object c is modified again!');
		console.log(this.name);
	}
}

c1.log();

console.log('--------');

// using ES5 - bind
const c2 = {
	name: 'object name is c',
	log: function() {
		this.name = 'object c is modified!';
		console.log(this.name);
		const setName = function(name) {
			this.name = name;
		}.bind(this);
		setName('object c is modified again!');
		console.log(this.name);
	}
}

c2.log();

console.log('--------');

// using ES6 - =>
const c3 = {
	name: 'object name is c',
	log: function() {
		this.name = 'object c is modified!';
		console.log(this.name);
		(name => this.name = name)('object c is modified again!');
		console.log(this.name);
	}
}

c3.log();

console.log('--------');
