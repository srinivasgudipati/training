// Get the appropriate content for the given fragment identifier
function getContent(fragmentId, callback) {

	// Content for each navigation link
	var partials = {
		home: "This is the home page. Welcome to my site",
		about: "This is the about page.",
		contact: "This is the contact page."
	};

	// Look up the partial for the given fragment id
	callback(partials[fragmentId]);
}

// Updates dynamic content based on the fragment identifier
function navigate() {
	// Get a reference to "content" div
	var contentDiv = document.getElementById('content');

	// Remove the '#' character from the fragment identifier
	var fragmentId = location.hash.substr(1);

	// Set the "content" div to display the current hash value
	getContent(fragmentId, function (content) {
		contentDiv.innerHTML = content;
	});

	setActiveLink(fragmentId);
}

// If no fragment identifier is provided
if (!location.hash) {
	// Default to #home

	location.hash = '#home';
}

// Navigate once to the initial hash value
navigate();

// Navigate whenever the fragment identifier value changes
window.addEventListener('hashchange', navigate);

function setActiveLink(fragmentId) {
	var links = document.getElementById("navbar").children;
	
	for (var i = 0; i < links.length; i++) {
		if (links[i].getAttribute("href").substr(1) === fragmentId) {
			links[i].setAttribute("class", "active");
		} else {
			links[i].removeAttribute("class");
		}
	}
}