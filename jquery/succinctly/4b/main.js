// Topic: Wait for the DOM to be ready

// Shortcut
$(function () {
	$('p').text('DOM is now loaded and ready to be manipulated.');
});