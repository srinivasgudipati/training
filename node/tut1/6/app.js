// pass by value
function change(b) {
	b = 2;
}

var a = 1;
change(a);
console.log('a = ', a);

// pass by reference
function changeObj(obj2) {
	obj2.firstname = 'Srinivas';
}

var obj1 = {firstname: 'Srini'};
changeObj(obj1);
console.log(obj1);
