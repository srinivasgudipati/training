// Topic: Array helpers: reduce

var colors = [
	{ name: 'red' },
	{ name: 'yellow' },
	{ name: 'blue' }
];

var colors1 = [];
var	colors2,
	colors3 = [];

// using for loop
for (var i = 0; i < colors.length; i++) {
	colors1.push(colors[i].name);
}

console.log(colors1);

// using map
colors2 = colors.map(function(color) {
	return color.name;
});

console.log(colors2);

// using reduce
colors3 = colors.reduce(function(prev, curr) {
	prev.push(curr.name);
	return prev;
}, []);

console.log(colors3);