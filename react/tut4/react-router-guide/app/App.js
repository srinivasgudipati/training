import React, { Component } from 'react'
import { Router, Route, Link, IndexLink, IndexRoute, hashHistory, browserHistory } from 'react-router'

function App() {
	return (
		<Router history={hashHistory}>
			<Route path='/' component={Container}>
				<IndexRoute component={Home} />
				<Route path='/feed' component={Feed}>
					<IndexRoute component={TwitterFeed} />
					<Route path='instagram' component={InstagramFeed} />
				</Route>
				<Route path='/admin' component={Admin}>
					<IndexRoute components={{main: Groups, sidebar: GroupsSidebar}} />
					<Route path='users' components={{main: Users, sidebar: UsersSidebar}}>
						<Route path=':userId' component={Profile} />
					</Route>
				</Route>
				<Route path='/about' component={About} />
				<Route path='*' component={NotFound} />
			</Route>
		</Router>
	);
}

const Admin = (props) => (
	<div>
		<div>
			{props.main}
		</div>
		<div>
			{props.sidebar}
		</div>
	</div>
);

const Users = (props) => (
	<div>
		<ul>
			<li><Link to={{pathname: '/admin/users/User 1', query: { message: 'Hello from Route Query' } }}>User 1</Link></li>
			<li><Link to='/admin/users/User 2'>User 2</Link></li>
			<li><Link to='/admin/users/User 3'>User 3</Link></li>
		</ul>
		{props.children}
	</div>
);

const Groups = () => (
	<ul>
		<li>Group 1</li>
		<li>Group 2</li>
		<li>Group 3</li>
	</ul>
);

const Profile = (props) => <h1>{props.params.userId} {props.location.query.message}</h1>;

const UsersSidebar = () => <h1>Users Sidebar</h1>;

const GroupsSidebar = () => <h1>Groups Sidebar</h1>;

const Home = () => <h1>Home</h1>;

const Feed = (props) => (
	<div>
		<Link to='/feed'>Twitter Feed</Link>&nbsp;|&nbsp;
		<Link to='/feed/instagram'>Instagram Feed</Link>
		{props.children}		
	</div>
);

const About = () => <h1>About</h1>;

const TwitterFeed = () => <h3>Twitter Feed</h3>;

const InstagramFeed = () => <h3>Instagram Feed</h3>; 

const NotFound = () => <h1>404.. This page is not found!</h1>;

const Nav = () => (
	<div>
		<IndexLink activeClassName='active' to='/'>Home</IndexLink>&nbsp;|&nbsp;
		<IndexLink activeClassName='active' to='/feed'>Feed</IndexLink>&nbsp;|&nbsp;
		<IndexLink activeClassName='active' to='/about'>About</IndexLink>&nbsp;|&nbsp;
		<IndexLink activeClassName='active' to='/admin'>Admin</IndexLink>
	</div>
);

const Container = (props) => <div><Nav />{props.children}</div>;

export default App;