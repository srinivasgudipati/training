// if the module does not have any dependencies, and it is just a collection of name/value pairs, then just pass an object literal to define():

define({
	color: "black",
	size: "unisize"
});