import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import {
	EMPLOYEE_CHANGED,
	EMPLOYEE_CREATE_SUCCESS,
	EMPLOYEE_LIST_FETCH_SUCCESS,
	EMPLOYEE_UPDATE_SUCCESS
} from './types';

export const employeeChanged = ({ prop, value }) => {
	return {
		type: EMPLOYEE_CHANGED,
		payload: { prop, value }
	};
};

export const employeeCreate = ({ name, phone, shift }) => {
	return dispatch => {
		const { currentUser } = firebase.auth();
		firebase.database().ref(`/users/${currentUser.uid}/employees`)
		.push({ name, phone, shift })
		.then(() => {
			dispatch({
				type: EMPLOYEE_CREATE_SUCCESS
			});
			Actions.employeeList({ type: 'reset' });
		});
	};
};

export const employeeListFetch = () => {
	const { currentUser } = firebase.auth();
	return dispatch => {
		firebase.database().ref(`/users/${currentUser.uid}/employees`)
		.on('value', snapshot => {
			dispatch({
				type: EMPLOYEE_LIST_FETCH_SUCCESS,
				payload: snapshot.val()
			});
		});
	};
};

export const employeeUpdate = (name, phone, shift, uid) => {
	const { currentUser } = firebase.auth();
	return dispatch => {
		firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
		.set({ name, phone, shift })
		.then(() => {
			dispatch({
				type: EMPLOYEE_UPDATE_SUCCESS
			});
			Actions.employeeList({ type: 'reset' });
		});
	};
};

export const employeeDelete = ({ uid }) => {
	const { currentUser } = firebase.auth();
	return () => {
		firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
		.remove()
		.then(() => {
			Actions.employeeList({ type: 'reset' });
		});
	};
};
