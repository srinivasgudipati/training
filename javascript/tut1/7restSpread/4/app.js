// Topic: Rest, spread operators

var lib = {
	calculate(...params) {
		return this.multiply(...params);
	},
	multiply(...params) {
		return params.reduce((total, param) => {
			return total * param;
		}, 1);
	}
}

console.log(lib.calculate(2, 3, 4, 6));