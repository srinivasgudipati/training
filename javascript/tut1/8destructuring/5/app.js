// Topic: Destructuring: Practical use

function signup({ password, dob, gender, email, username }) {
	console.log('username: ', username);
	console.log('password: ', password);
	console.log('dob: ', dob);
	console.log('gender: ', gender);
	console.log('email: ', email);
}

const user = {
	username: 'user1',
	password: 'pass1',
	dob: 'dob1',
	gender: 'gender1',
	email: 'email1'
};

signup(user);