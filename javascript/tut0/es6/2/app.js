// Topic: Fat arrows

// ES5
const c = {
	name: 'c',
	log: function() {
		console.log(this.name); // 'this' refers to object 'c'
		(function() {
			console.log(this.name); // 'this' refers to global object
		})();
	}
};

c.log();

// ES6
const d = {
	name: 'd',
	log: function() {
		console.log(this.name); // 'this' refers to object 'd'
		(() => 	{
			console.log(this.name) // 'this' refers to object'd'
		})();
	}
};

d.log();

// ES6
const e = {
	name: 'e',
	log: () => {
		console.log(this.name); // 'this' refers to global object
		(() => 	{
			console.log(this.name) // 'this' refers to global object
		})();
	}
};

e.log();