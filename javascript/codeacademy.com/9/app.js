// Here is the array we will store results in
var multiples = [];

function multiplesOf(base, i) {
  // Base case
  if (i == 0) {
	console.log(multiples);
  }
  // Recursive case
  else {
    multiples[i - 1] = base * i;
	multiplesOf(base, i - 1);
  }	
}

multiplesOf(5, 3);