// Topic: Array helpers: find

var users = [
	{ id: 1, name: 'Roger' },
	{ id: 2, name: 'Rafael' },
	{ id: 3, name: 'Novak' },
	{ id: 4, name: 'Andy' },
	{ id: 5, name: 'Roger' }
];

var user1,
	user2;

for (var i = 0; i < users.length; i++) {
	if (users[i].name === 'Roger') {
		user1 = users[i];
		break;
	}
}

console.log(user1);

user2 = users.find(function(user) {
	return user.name === 'Roger'
});

console.log(user2);