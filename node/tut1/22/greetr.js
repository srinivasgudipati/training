'use strict';

var Emitter = require('events');

class Greetr extends Emitter {
	constructor() {
		super();
		this.greeting = 'Hello';
	}

	greet(data) {
		console.log(this.greeting);
		this.emit('greet', data);
	}
}

module.exports = Greetr;