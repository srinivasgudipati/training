define(['backbone'], function(Backbone) {
	
	var HomeView = Backbone.View.extend({
		render: function() {
			this.$el.html("Home");

			return this;
		}
	});

	return HomeView;
});