define([
	'models/car',
	'collections/cars',
	'views/carsView',
	'views/homeView'], function(Car, Cars, CarsView, HomeView) {
	
	var initialize = function() {

		var AppRouter = Backbone.Router.extend({
			routes: {
				"": "viewHome",
				"cars": "viewCars"
			},

			viewHome: function() {
				var homeView = new HomeView();
				this.loadView(homeView);
			},

			viewCars: function() {
				var cars = new Cars([
					new Car({ name: "Toyota" }),
					new Car({ name: "Honda" }),
					new Car({ name: "Mercedez" })			
				]);

				var carsView = new CarsView({ collection: cars });
				this.loadView(carsView);
			},

			// We use this method to prevent memory leaks. When you replace
			// the content of a DOM element with a new view, the old view is 
			// still in the memory. So, we need to remove it explicitly. 
			//
			// Here we use a private field (_currentView) to keep track of the
			// current view. 
			loadView: function(view){
				// If the currentView is set, remove it explicitly.
				if (this._currentView) {
					this._currentView.remove();
				}

				$("#container").html(view.render().$el);
				
				this._currentView = view;
			}			
		});

		var router = new AppRouter();
		Backbone.history.start();

		var NavView = Backbone.View.extend({
			events: {
				"click": "onClick",
			},

			onClick: function(e) {
				router.navigate($(e.target).attr("data-url"), { trigger: true });
			}
		});

		new NavView({ el: "#nav" }).render();
	}

	return {initialize: initialize};
});