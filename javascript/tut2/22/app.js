function add(num1) {
	return function(num2) {
		return num1 + num2;
	}
}

var add5 = add(5);
var add10 = add(10);

console.log(add5(3));
console.log(add5(12));
console.log(add10(3));
console.log(add10(12));