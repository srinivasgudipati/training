// Topic: jQuery chaining

(function ($) {
	$('a').text('google') // Sets text to google and then returns $('a')
	.attr('href', 'http://www.google.com') // Sets the href attribute and then returns $('a')
	.addClass('red'); // Sets the class and then returns $('a')
})(jQuery);