// Topic: Array helpers: find

function findWhere(array, criteria) {
	var property = Object.keys(criteria)[0];
	return array.find(function (element) {
		return element[property] === criteria[property];
	});
}

var ladders = [
	{ id: 1, name: 'Srinivas', country: 'India' },
	{ id: 3, name: 'John', country: 'U.S.A' }
];

console.log(findWhere(ladders, { country: 'India' }));