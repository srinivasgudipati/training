var Comment = Backbone.Model.extend({
	urlRoot: "/api/comments",
	idAttribute: "commentId" // Default is set to "id". Override like this if using different id
});

// Create new comment
var newComment = new Comment();
newComment.set("author", "Samiksha Gudipati");
newComment.set("text", "The Dark Clouds");
newComment.save();

// Get all comments
var comments = new Comment();
console.log(comments.fetch());

// Get one comment
var comment = new Comment({ commentId: 1 });
console.log(comment.fetch());

// Delete comment
comment.destroy();