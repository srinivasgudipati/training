import React, { Component } from 'react';
import { Text } from 'react-native';
import firebase from 'firebase';

import { Button, Input, Card, CardSection, Spinner } from './common';

class LoginForm extends Component {
	state = { email: '', password: '', error: '', loading: false };
	
	handleButtonPress = this.handleButtonPress.bind(this);
	handleEmailChange = this.handleEmailChange.bind(this);
	handlePasswordChange = this.handlePasswordChange.bind(this);
	handleLoginSuccess = this.handleLoginSuccess.bind(this);
	handleLoginFail = this.handleLoginFail.bind(this);

	handleButtonPress() {
		const { email, password } = this.state;

		this.setState({ error: '', loading: true });

		firebase.auth().signInWithEmailAndPassword(email, password)
			.then(this.handleLoginSuccess)
			.catch(() => {
				firebase.auth().createUserWithEmailAndPassword(email, password)
					.then(this.handleLoginSuccess)
					.catch(this.handleLoginFail);
			});
	}

	handleEmailChange(email) {
		this.setState({ email });
	}

	handlePasswordChange(password) {
		this.setState({ password });
	}

	handleLoginSuccess() {
		this.setState({
			email: '',
			password: '',
			error: '',
			loading: false
		});
	}

	handleLoginFail() {
		this.setState({
			password: '',
			error: 'Authentication failed',
			loading: false
		});
	}

	renderButton() {
		if (this.state.loading) {
			return <Spinner />;
		}

		return (
			<Button onPress={this.handleButtonPress}>
				Log in
			</Button>
		);
	}

	render() {
		return (
			<Card>
				<CardSection>
					<Input
						placeholder='user@gmail.com'
						label='Email'
						value={this.state.email}
						onChangeText={this.handleEmailChange}
					/>
				</CardSection>
				<CardSection>
					<Input
						secureTextEntry
						placeholder='password'
						label='Password'
						value={this.state.password}
						onChangeText={this.handlePasswordChange}
					/>
				</CardSection>
				<Text style={styles.errorTextStyle}>{this.state.error}</Text>
				<CardSection>
					{this.renderButton()}
				</CardSection>
			</Card>
		);
	}
}

const styles = {
	errorTextStyle: {
		alignSelf: 'center',
		fontSize: 18,
		color: 'red'
	}
};

export default LoginForm;
