// function arguments can be declared to be of particular type
// typescript expects the function to be called with the default arguments
// optional arguments should be at the end of the list of arguments
// return type of a function can be specified
function add(num1, num2, num3, num4) {
    if (num3 === void 0) { num3 = 0; }
    return num1 + num2 + num3;
}
console.log(add(1, 2, 3, 4));
