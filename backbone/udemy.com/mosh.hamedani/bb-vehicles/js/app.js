define([
	'underscore',
	'backbone',
	'collections/vehicles',
	'models/car',
	'views/newVehicleView',
	'views/vehiclesView'], function(_, Backbone, Vehicles, Car, NewVehicleView, VehiclesView) {
		
		function initialize() {
			var vehicles = new Vehicles([
				new Car({ registrationNumber: "XLI887", colour: "Blue"}),
				new Car({ registrationNumber: "ZNP123", colour: "Blue"}),
				new Car({ registrationNumber: "XUV456", colour: "Gray"})
			]);

			var bus = _.extend({}, Backbone.Events);

			$("#container")
				.append(new NewVehicleView({ bus: bus }).render().$el)
				.append(new VehiclesView({ collection: vehicles, bus: bus }).render().$el);
		}

		return {
			initialize: initialize
		}
});