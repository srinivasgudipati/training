var a = {
	log: function() {
		console.log(this); // Points to object 'a'
		(function() {
			console.log(this); // Points to 'node' object
		})();
	}
}

var b = {
	log: function() {
		console.log(this); // Points to object 'b'
		(() => {
			console.log(this); // Points to object 'b'
		})();
	}
}

var c = {
	log: () => {
		console.log(this); // Points to empty object
		(() => {
			console.log(this); // Points to empty object
		})();
	}
}

console.log(this); // Points to empty object
console.log("=======");
a.log();
console.log("=======");
b.log();
console.log("=======");
c.log();