// State argument is not application state, only the state this reducer is responsible for
export default function(state = 0, action) {
	switch(action.type) {
		case 'INCREMENT':
			return state + 1;
		case 'DECREMENT':
			return state - 1;
		default:
			return state;
	}
}