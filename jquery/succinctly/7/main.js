// Topic: Destructive method

(function ($) {
	var theText = $('p').text('jQuery') // Sets text to jQuery and then returns $('p')
					.text() // Returns 'jQuery' string not the jQuery object and jQuery chain is broken
	console.log(theText);
})(jQuery);