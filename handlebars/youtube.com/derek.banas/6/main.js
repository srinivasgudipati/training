// Topic: Pass options that are available to all helper functions within a template

var template = Handlebars.compile('{{sayHello}} {{name}} - {{showLanguage}}');

Handlebars.registerHelper('showLanguage', function (options) {
	return options.data.lang;
});

Handlebars.registerHelper('sayHello', function (options) {
	switch(options.data.lang) {
		case 'es':
		return 'Hola';
		case 'fr':
		return 'bonjour';
		default:
		return 'Hello';
	}
});

// Second parameter is avaialable to all the helper functions as options
$('#data').html(template({
	name: 'Srinivas'
}, {data: {
	lang: 'es'
	}
}));