define([
	'underscore',
	'backbone'], function(_, Backbone) {
		var Vehicle = Backbone.Model.extend({
			idAttribute: "registrationNumber",

			urlRoot: "/api/vehicles",

			start: function() {
				console.log("Vehicle started");
			},

			validate: function(attrs) {
				if(!attrs.registrationNumber)
					return "registrationNumber cannot be null";
			}
		});

		return Vehicle;
});