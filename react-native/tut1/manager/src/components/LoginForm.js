import React, { Component } from 'react';
import { Text } from 'react-native';
import { connect } from 'react-redux';

import { Card, CardSection, Input, Button, Spinner } from './common';
import { emailChanged, passwordChanged, loginUser } from '../actions';

class LoginForm extends Component {
	handleEmailChange = this.handleEmailChange.bind(this);
	handlePasswordChange = this.handlePasswordChange.bind(this);
	handleButtonPress = this.handleButtonPress.bind(this);

	handleEmailChange(text) {
		this.props.emailChanged(text);
	}

	handlePasswordChange(text) {
		this.props.passwordChanged(text);
	}

	handleButtonPress() {
		const { email, password } = this.props;
		this.props.loginUser({ email, password });
	}

	renderButton() {
		if (this.props.loading) {
			return <Spinner />;
		}

		return (
			<Button onPress={this.handleButtonPress}>
				Login
			</Button>
		);
	}

	render() {
		return (
			<Card>
				<CardSection>
					<Input
						label='Email'
						placeholder='Email'
						onChangeText={this.handleEmailChange}
						value={this.props.email}
					/>
				</CardSection>

				<CardSection>
					<Input
						secureTextEntry
						label='Password'
						placeholder='Password'
						onChangeText={this.handlePasswordChange}
						value={this.props.password}
					/>
				</CardSection>
				<Text style={styles.errorTextStyle}>{this.props.error}</Text>
				<CardSection>
					{this.renderButton()}
				</CardSection>
			</Card>
		);
	}
}

const styles = {
	errorTextStyle: {
		fontSize: 20,
		color: 'red',
		textAlign: 'center'
	}
};

const mapStateToProps = (state) => {
	const { email, password, user, error, loading } = state.auth;
	return { email, password, user, error, loading };
};

export default connect(mapStateToProps,
	{ emailChanged, passwordChanged, loginUser }
	)(LoginForm);
