var Emitter = require('events');
var eventConfig = require('./config').events;

var emtr = new Emitter();

emtr.on(eventConfig.GREET, function() {
	console.log('Someone, somewhere said hello!');
});

emtr.on(eventConfig.GREET, function() {
	console.log('A greeting occured');
});

console.log('hello');
emtr.emit(eventConfig.GREET);