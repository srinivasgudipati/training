var a = {
	m1: function() {
		console.log(this); // Points to object 'a'
	},
	m2: function() {
		this.m1();
	}
}

var b = {
	m1: function() {
		console.log(this); // Points to 'node' object
	},
	m2: function() {
		var m3 = this.m1;
		m3();
	}
}

var c = {
	m1: function() {
		console.log(this); // Points to object 'c'
	},
	m2: function() {
		var m3 = () => {
			this.m1();
		} 
		m3();
	}
}

var d = {
	m1: function() {
		console.log(this); // Points to object 'd'
	},
	m2: function() {
		var m3 = this.m1.bind(this);
		m3();
	}
}

a.m2();
console.log("=======");
b.m2();
console.log("=======");
c.m2();
console.log("=======");
d.m2();