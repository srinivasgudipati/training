interface Person {
  firstname: String;
  lastname: String;
  getFullName(): String;
}

class Foo implements Person {
  firstname: String;
  lastname: String;
  getFullName(): String {
    return `${this.firstname} ${this.lastname}`;
  }
}

var foo = new Foo();
foo.firstname = 'Srinivas';
foo.lastname = 'Gudipati';

console.log(foo.getFullName());

var aPerson: Person = new Foo(); // polymorphism

var obj = {
  firstname: 'Sanjay',
  lastname: 'Gudipati',
  age: 6,
  getFullName: function() {
    return `${this.firstname} ${this.lastname}`;
  }
};

aPerson = obj; // duck typing
console.log(aPerson.getFullName());