export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

export const EMPLOYEE_CHANGED = 'employee_changed';
export const EMPLOYEE_CREATE_SUCCESS = 'employee_create_success';
export const EMPLOYEE_LIST_FETCH_SUCCESS = 'employee_list_fetch_success';
export const EMPLOYEE_UPDATE_SUCCESS = 'employee_update_success';
