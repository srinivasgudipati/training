define([
	'backbone',
	'models/car'], function(Backbone, Car) {
	
	var Cars = Backbone.Collection.extend({
		model: Car
	});

	return Cars;
});