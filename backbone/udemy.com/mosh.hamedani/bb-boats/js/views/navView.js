define([], function() {
	var NavView = Backbone.View.extend({
		events: {
			"click": "onClick",

			onClick: function(e) {
				router.navigate($(e.target).attr("data-url"), { trigger: true });
			}
		}
	});
});