// Topic: Events

var obj = {name: "Srinivas"};

_.extend(obj, Backbone.Events);

//Bind a callback function to the object "obj". The callback will be invoked whenever the event "alert" is triggered
obj.on("alert", function(msg) {
	alert("Triggered " + msg);
});

// Trigger callbacks for the given event
obj.trigger("alert", "an event");