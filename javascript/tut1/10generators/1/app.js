// Topic: for of

var colors = ['red', 'green', 'yellow'];

for (let color of colors) {
	console.log(color);
}

var numbers = [1, 2, 3, 4];

let total = 0;
for (let number of numbers) {
	total += number;
}

console.log(total);