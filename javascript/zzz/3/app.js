// Topic: Asynchronous calls

function getList(callback) {
	setTimeout(function() {
		callback([1, 2, 3, 4, 5, 6]);
	}, 2000);
}

function getEvens(list) {
	return list.filter(function(num) {
		return num % 2 === 0;
	});
}

function addFour(list) {
	return list.map(function(num) {
		return num + 4;
	});
}

function sum(list) {
	var res = getEvens(list);
	res = addFour(res);
	console.log(res.reduce(function(sum, num) {
		return sum + num;
	}, 0));
}

getList(sum);

console.log("waiting....");