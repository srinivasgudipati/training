// Topic: Destructuring

var points = [
	[4, 6],
	[10, 20],
	[20, 30]
];

console.log(points);

// ES5
var points1 = points.map(function(point) {
	var point1 = {x: point[0], y: point[1]};
	return point1;
});

console.log(points1);

// ES6
const points2 = points.map(([ x, y ]) => {
	return { x, y };
});

console.log(points2);