// Topic: Objects: keys

var foo = { a: 1, b: 2 };

var bar = Object.create(foo);
bar.c = 3;
bar.d = 4;

console.log(_.keys(bar)); // ["c", "d"] - returns all the names of the object's own enumerable properties