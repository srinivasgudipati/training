// Topic: Default Function Arguments

function User(id) {
	this.id = id;
}

function generateId() {
	return Math.random() * 9999;
}

// ES5
function createAdminUser(user) {
	user.admin = true;

	return user;
}

console.log(createAdminUser(new User(generateId())));

// ES6
function createAdminUser1(user = new User(generateId())) {
	user.admin = true;

	return user;
}

console.log(createAdminUser1(new User(1)));