// Topic: Model: urlRoot

var Post = Backbone.Model.extend({
	urlRoot: 'http://jsonplaceholder.typicode.com/posts'
});

var post = new Post({ id: 2 });

post.fetch({
	success: function(data) {
		console.log(data); // Backbone object
	}
});

post.fetch().then(function(data) {
	console.log(data); // JSON object
})