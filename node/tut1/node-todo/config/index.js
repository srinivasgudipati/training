var configValues = require('./config');

module.exports = {
	getDbConnectionString: function() {
		return `mongodb://${configValues.uname}:${configValues.pwd}@ds143151.mlab.com:43151/nodetodo-db`;
	}
};