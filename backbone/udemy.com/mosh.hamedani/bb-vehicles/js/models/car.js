define([
	'underscore',
	'backbone',
	'models/vehicle'], function(_, Backbone, Vehicle) {
		var Car = Vehicle.extend({
			start: function() {
				console.log("Car with registration number " + this.get("registrationNumber") + " started");
			}
		});

		return Car;
});