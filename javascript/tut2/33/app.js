var c = {
	foo: "bar",
	func: function() {
		var self = this;
		console.log("outer function: this.foo =", this.foo);
		console.log("outer function: self.foo =", self.foo);
		(function() {
			console.log("inner function: this.foo =", this.foo);
			console.log("outer function: self.foo =", self.foo);
			(function() {
				console.log("inner inner function: this.foo =", this.foo);
				console.log("inner inner function: self.foo =", self.foo);
			})();
		})();
	}
}

c.func();
console.log("global object: this.foo =", this.foo);