// Topic: Synchronous calls

function getList() {
	return [1, 2, 3, 4];
}

function getEvens() {
	return getList().filter(function(num) {
		return num % 2 === 0;
	});
}

function addFour() {
	return getEvens().map(function(num) {
		return num + 4;
	});
}

function sum() {
	return addFour().reduce(function(sum, num) {
		return sum + num;
	}, 0);
}

console.log("sum = ", sum());