// Topic: Objects: allKeys

var foo = { a: 1, b: 2 };

var bar = Object.create(foo);
bar.c = 3;
bar.d = 4;

console.log(_.allKeys(bar)); // ["c", "d", "a", "b"] - returns all the names of the object's own and inherited properties