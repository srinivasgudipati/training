// Topic: Arrays: range

console.log(_.range(10));
console.log(_.range(1, 10));
console.log(_.range(1, 10, 2));
console.log(_.range(-1, -10, -1));
console.log(_.range(0));