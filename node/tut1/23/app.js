var buf = new Buffer('Hello', 'utf8');
console.log(buf); // binary data represented in hexadecimal
console.log(buf.toString()); // converted back to string
/*
	Buffer can be converted to JSON object which contains array of numbers.
	Each number corresponds to unicode character set for each letter in the buffer
*/
console.log(buf.toJSON()); 
console.log(buf[2]);

buf.write('wor'); // Overwrite buffer
console.log(buf.toString()); // converted back to string