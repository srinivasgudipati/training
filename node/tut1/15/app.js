function Foo() {
	this.prop1 = 'prop1';
}

function Fum() {
	this.prop2 = 'prop2';
}

Fum.prototype.log = function() {
	console.log(this.prop2);
}

Foo.prototype = new Fum();

var foo = new Foo();
foo.log();
console.log(foo.prop2);