// Topic: Model creation

// To create a model, call the extend method on Backbone.Model method which returns JS constructor function
// Optional object can be passed to extend method which determines configuration of the model
var Song = Backbone.Model.extend({
	// initialize method is automatically called by backbone when an object of this type is instantiated
	initialize: function() {
		console.log("A new song is created");
	}
});

var Song1 = function(name) {
	this.name = "Srinivas";
}
Song1.prototype.initialize = function() {
	console.log("A new song is created");
}

var song = new Song({name: "Srinivas"}); // creates a new instance of Song
var song1 = new Song1();
song1.initialize();