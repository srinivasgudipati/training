import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import _ from 'lodash';

import SearchBar from './components/search_bar';
import VideoDetail from './components/video_detail';
import VideoList from './components/video_list';

const API_KEY = 'AIzaSyBd_rkfjm1Rbcrv_odMdmftI06shXL9sgc';

class App extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			videos: [],
			selectedVideo: null
		};

		this.onVideoSearch('Narendra Modi');

		this.onVideoSelect = this.onVideoSelect.bind(this);
		this.onVideoSearch = this.onVideoSearch.bind(this);
	}

	onVideoSelect(selectedVideo) {
		this.setState({ selectedVideo });
	}

	onVideoSearch(term) {
		YTSearch({ key: API_KEY, term: term }, videos => {
			this.setState({
				videos: videos,
				selectedVideo: videos[0]
			});
		});
	}

	render() {
		const videoSearch = _.debounce(term => {
			this.onVideoSearch(term);
		}, 300);
		return (
			<div>
				<SearchBar onVideoSearch={videoSearch}/>
				<VideoDetail video={this.state.selectedVideo} />
				<VideoList
					videos={this.state.videos}
					onVideoSelect={this.onVideoSelect} />
			</div>
		);
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('root')
);