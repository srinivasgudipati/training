var person = new Object();

person["firstname"] = "Srinivas";
person["lastname"] = "Gudipati";

var firstnameProperty = "firstname";

console.log(person);
console.log(person[firstnameProperty]);

console.log(person.firstname);
console.log(person.lastname);

person.address = new Object();

person.address.street = "100 Main St.";
person.address.city = "Pleasanton";
person.address.state = "CA";
person.address.zip = "41211";

console.log(person.address.street);
console.log(person.address["city"]);
console.log(person["address"]["state"]);