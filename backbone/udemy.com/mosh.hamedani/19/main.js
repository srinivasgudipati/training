// Topic: Router

var AlbumsView = Backbone.View.extend({
	render: function() {
		this.$el.html("ALBUMS VIEW");

		return this;
	}
});

var ArtistsView = Backbone.View.extend({
	render: function() {
		this.$el.html("ARTISTS VIEW");

		return this;
	}
});

var GenresView = Backbone.View.extend({
	render: function() {
		this.$el.html("GENRES VIEW");

		return this;
	}
});

var DefaultView = Backbone.View.extend({
	render: function() {
		this.$el.html("OOOPS");

		return this;
	}
});

var AppRouter = Backbone.Router.extend({
	routes: {
		// key is the route param and value is the router handler
		"albums": "viewAlbums",
		"artists": "viewArtists",
		"genres": "viewGenres",
		"*other": "defaultRoute"
	},

	viewAlbums: function() {
		var view = new AlbumsView({ el: "#container" });
		view.render();
	},

	viewArtists: function() {
		var view = new ArtistsView({ el: "#container" });
		view.render();
	},

	viewGenres: function() {
		var view = new GenresView({ el: "#container" });
		view.render();
	},

	defaultRoute: function() {
		var view = new DefaultView({ el: "#container" });
		view.render();
	}
});

var router = new AppRouter(); // create AppRouter instance
Backbone.history.start();

var NavView = Backbone.View.extend({
	events: {
		"click": "onClick"
	},

	onClick: function(e) {
		var $li = $(e.target);
		router.navigate($li.attr("data-url"), { trigger: true });
	}
});

var navView = new NavView({ el: "#nav" });
