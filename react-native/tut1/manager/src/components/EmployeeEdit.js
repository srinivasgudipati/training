import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { text } from 'react-native-communications';

import EmployeeForm from './EmployeeForm';
import { Card, CardSection, Button, Confirm } from './common';
import { employeeChanged, employeeUpdate, employeeDelete } from '../actions';

class EmployeeEdit extends Component {
	state = { showModal: false };

	componentWillMount() {
		_.each(this.props.employee, (value, prop) => {
			this.props.employeeChanged({ prop, value });
		});
	}

	handleButtonPress = this.handleButtonPress.bind(this);
	handleTextPress = this.handleTextPress.bind(this);
	handleFirePress = this.handleFirePress.bind(this);
	handleDecline = this.handleDecline.bind(this);
	handleAccept = this.handleAccept.bind(this);

	handleButtonPress() {
		const { name, phone, shift } = this.props;
		this.props.employeeUpdate(name, phone, shift, this.props.employee.uid);
	}

	handleTextPress() {
		const { phone, shift } = this.props;
		text(phone, `Your upcoming shift is on ${shift}`);
	}

	handleFirePress() {
		this.setState({ showModal: !this.state.showModal });
	}

	handleDecline() {
		this.setState({ showModal: false });
	}

	handleAccept() {
		const { uid } = this.props.employee;
		this.props.employeeDelete({ uid });
	}

	render() {
		return (
			<Card>
				<EmployeeForm />

				<CardSection>
					<Button onPress={this.handleButtonPress}>
						Save Chages
					</Button>
				</CardSection>

				<CardSection>
					<Button onPress={this.handleTextPress}>
						Text Schedule
					</Button>
				</CardSection>

				<CardSection>
					<Button onPress={this.handleFirePress}>
						Fire Employee
					</Button>
				</CardSection>
				<Confirm
					visible={this.state.showModal}
					onDecline={this.handleDecline}
					onAccept={this.handleAccept}
				>
					Are you sure you want to delete this employee?
				</Confirm>
			</Card>
		);
	}
}

const mapStateToProps = state => {
	const { name, phone, shift } = state.employeeForm;

	return {
		name, phone, shift
	};
};

export default connect(mapStateToProps,
	{ employeeChanged, employeeUpdate, employeeDelete }
)(EmployeeEdit);
