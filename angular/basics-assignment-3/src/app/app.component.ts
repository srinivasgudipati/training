import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private showDetails = true;
  clicks = [];

  onToggleDetails() {
    this.showDetails = !this.showDetails
    this.clicks.push(this.clicks.length + 1);
  }
}
