function foo(num, flag) {
	if( num <= 2) {
		return;
	}
	console.log('--Begin--');
	console.log(flag);
	console.log(num);
	console.log('--End--');
	foo(num-1, 'first call') + foo(num-2, 'second call');
}

foo(6);

/*
1.foo(6) {
	// --Begin--
	// undefined
	// 6
	// --End--
	foo(5, 'first call') + foo(4, 'second call');
2.	foo(5, 'first call') {
		// --Begin--
		// first call
		// 5
		// --End--
		foo(4, 'first call') + foo(3, 'second call');
3.		foo(4, 'first call') {
			// --Begin--
			// first call
			// 4
			// --End--
			foo(3, 'first call') + foo(2, 'second call');
4.			foo(3, 'first call') {
				// --Begin--
				// first call
				// 3
				// --End--
				foo(2, 'first call') + foo(1, 'second call');
5.				foo(2, 'first call') {
					return;
				}
6.				foo(1, 'second call') {
					return;
				}
			}
7.			foo(2, 'second call') {
				return;
			}
		};
8.		foo(3, 'second call') {
			// --Begin--
			// second call
			// 3
			// --End--
			foo(2, 'first call') + foo(1, 'second call');
9.			foo(2, 'first call') {
				return;
			}
10.			foo(1, 'second call') {
				return;
			}
		}
	};
11.	foo(4, 'second call') {
		// --Begin--
		// second call
		// 4
		// --End--
		foo(3, 'first call') + foo(2, 'second call');
12.		foo(3, 'first call') {
			// --Begin--
			// first call
			// 3
			// --End--
			foo(2, 'first call') + foo(1, 'second call');
13.			foo(2, 'first call') {
				return;
			}
14.			foo(1, 'second call') {
				return;
			}			
		};
		foo(2, 'second call') {
			return;
		};
	};
};

*/