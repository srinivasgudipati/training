// Topic: Array helpers: reduce

var numbers = [10, 20, 30];

var sum = 0;

//using for loop
for (var i = 0; i < numbers.length; i++) {
	sum += numbers[i];
}

console.log(sum);

console.log('---');

// using reduce helper
sum = numbers.reduce(function(prev, curr) {
	return prev + curr;
}, 0);

console.log(sum);