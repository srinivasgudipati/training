var buffer = new ArrayBuffer(8);
var view = new Int32Array(buffer);

view[0] = 123;
view[1] = 250;
view[2] = 300; // ignored

console.log(view);