var Vehicle = Backbone.Model.extend({
	idAttribute: "registrationNumber",

	urlRoot: "/api/vehicles",

	start: function() {
		console.log("Vehicle started");
	},

	validate: function(attrs) {
		if(!attrs.registrationNumber)
			return "registrationNumber cannot be null";
	}
});

var Car = Vehicle.extend({
	start: function() {
		console.log("Car with registration number " + this.get("registrationNumber") + " started");
	}
});

var Vehicles = Backbone.Collection.extend({
	model: Vehicle
});

var vehicles = new Vehicles([
	new Car({ registrationNumber: "XLI887", colour: "Blue"}),
	new Car({ registrationNumber: "ZNP123", colour: "Blue"}),
	new Car({ registrationNumber: "XUV456", colour: "Gray"})
]);

var VehicleView = Backbone.View.extend({
	tagName: "li",

	className: "vehicle",

	events: {
		"click .delete": "onClick"
	},

	initialize: function() {
		this.model.on("change", this.render, this);
	},

	onClick: function() {
		this.remove();
	},

	render: function() {
		var template = _.template($("#vehicleTemplate").html());

		this.$el.html(template(this.model.toJSON()));
		this.$el.attr("id", this.model.get("registrationNumber"));
		this.$el.attr("data-color", this.model.get("colour"));
		return this;
	}
});

var VehiclesView = Backbone.View.extend({
	tagName: "ul",

	id: "vehicles",

	initialize: function(options) {
		this.bus = options.bus;
		// We pass "this" as the third argument so inside
		// onNewVehicle method, we can access it. If 
		// you don't set the "this" here, and you access
		// "this" inside onNewVehicle, it won't be pointing
		// to the view itself. This is how Javascript works.
		this.bus.on("newVehicle", this.onNewVehicle, this);
		this.collection.on("add", this.onNewVehicle, this);
		this.collection.on("remove", this.onRemove, this);
	},

	onNewVehicle: function(vehicle) {
		this.$el.append(new VehicleView({ model: vehicle }).render().$el);
	},

	onRemove: function(vehicle) {
		this.$el.find("li#" + vehicle.get("registrationNumber")).remove();
	},

	render: function() {
		var self = this;

		this.collection.each(function(vehicle) {
			var vehicleView = new VehicleView({ model: vehicle });
			self.$el.append(vehicleView.render().$el);
		});

		return this;
	}
});

var bus = _.extend({}, Backbone.Events);

var NewVehicleView = Backbone.View.extend({

	initialize: function(options) {
		this.bus = options.bus;
	},

	events: {
		"click .add": "onClick"
	},

	onClick: function() {
		var input = this.$el.find(".registration-number");
		this.bus.trigger("newVehicle", new Vehicle({ registrationNumber: input.val()}));

		input.val("");
	},

	render: function() {
		var template = _.template($("#newVehicleTemplate").html());
		this.$el.html(template());

		return this;
	}
});

$("#container")
	.append(new NewVehicleView({ bus: bus }).render().$el)
	.append(new VehiclesView({ collection: vehicles, bus: bus }).render().$el);