// Topic: Array helpers: reduce

function balancedParens(str) {
	return !str.split('').reduce(function(prev, curr) {
		if(prev < 0) return prev;
		if(curr === '(') prev++;
		if(curr === ')') prev--;
		return prev;
	}, 0);
}

console.log(balancedParens("()"))