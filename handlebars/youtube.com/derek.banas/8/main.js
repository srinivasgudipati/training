// Topic: Block Helpers with alternate templates

var template = Handlebars.compile($("#the-template").html());

Handlebars.registerHelper("areEqual", function(num1, num2, options) {
	if (num1 === num2) {
		return options.fn(this);
	} else {
		return options.inverse(this);
	}
});

$("#data").html(template());