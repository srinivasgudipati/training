// Topic: Collections: map

var arr1 = [1, 2, 3];
var arr2 = [[1, 2], [3, 4]];
var obj = { a: 1, b: 2, c:3 };

console.log(arr1.map(function (num) {
	return num * 3;
}));

console.log(_.map(arr1, function (num) {
	return num * 3;
}));

console.log(_.map(arr2, _.first));

console.log(_.map(obj, function (num, key) {
	return num * 3;
}));
