class Person {
  private firstname: string;
  private lastname: string;

  constructor(firstname: string, lastname: string) {
    this.firstname = firstname;
    this.lastname = lastname;
  }

  getFirstName() {
    return this.firstname;
  }

  setFirstName(firstname) {
    this.firstname = firstname;
  }

  getLastName() {
    return this.lastname;
  }

  setLastName(lastname) {
    this.lastname = lastname;
  }

  getFullName() {
    return this.firstname + ' ' + this.lastname;
  }
}

var aPerson = new Person("Srinivas", "Gudipati");

console.log(aPerson.getFirstName());
aPerson.setFirstName('Sanjay');
console.log(aPerson.getFirstName());
