// Topic: class

class Car {
	constructor(options) {
		this.title = options.title;
	}

	drive() {
		return 'vroooooommmmm vrooooommmmm.....';
	}
}

class Toyota extends Car {
	constructor(options) {
		super(options);
	}

	honk() {
		return 'beep';
	}
}

const toyota = new Toyota({color: 'Red', title: "New Driver"});

console.log(toyota.drive());
console.log(toyota.honk());
console.log(toyota.title);