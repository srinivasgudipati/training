var Foo = /** @class */ (function () {
    function Foo() {
    }
    Foo.prototype.getFullName = function () {
        return this.firstname + " " + this.lastname;
    };
    return Foo;
}());
var foo = new Foo();
foo.firstname = 'Srinivas';
foo.lastname = 'Gudipati';
console.log(foo.getFullName());
var aPerson = new Foo(); // polymorphism
var obj = {
    firstname: 'Sanjay',
    lastname: 'Gudipati',
    age: 6,
    getFullName: function () {
        return this.firstname + " " + this.lastname;
    }
};
aPerson = obj; // duck typing
console.log(aPerson.getFullName());
