// Topic: Array helpers: forEach

var colors = [ 'red', 'blue', 'green' ];

// using for loop
for (var i = 0; i < colors.length; i++) {
	console.log(colors[i]);
}

// using array helper
colors.forEach(function(color) {
	console.log(color);
});