// Topic: Views

var SongView = Backbone.View.extend({
	render: function() {
		this.$el.html("Hello World"); // this.$el is jQuery object that contains the view's DOM element.

		return this; // Returning reference to the view helps with chaining method calls
	}
});

var songView = new SongView({ el: "#container" }); 	/*
													While instantiating the view, you can specify the DOM element to which 
													this view should attach to.
													We are doing this by setting 'el' property to a jQuery selector string 
													that references the DOM element that this field owns.
													In this case we are telling our view that there is an HTML element with an
													id of "container" in our HTML mark up
													This view will be responsible for rendering the content 
													inside the "container" as well as responding to any events raised from it
													*/
songView.render();