// Topic: Collections: each

var numArray = [1, 2, 3];

console.log(_.each(numArray, callback)); // [1, 2 ,3] - returns original list

var objectArray = { a: 1, b: 2, c: 3 };
console.log(_.each(objectArray, callback)); // { a: 1, b: 2, c: 3 } - returns original list

numArray.forEach(callback); // ES6 - returns null

function callback(element, index, array) {
	console.log(element * 2);
	console.log(index);
	console.log(array);
}