// Topic: Models Validation

var User = Backbone.Model.extend({
	validate: function(attrs) {
		if(!attrs.username) {
			return "Username is required";
		}
		if(!attrs.password) {
			return "Password is required";
		}
		if(attrs.password.length < 8) {
			return "Password must have at least 8 characters";
		}
	}
});

var user = new User();
user.set("username", "Srinivas");
user.set("password", "Hello");
console.log(user.isValid()); // validate method is called when isValid method is invoked on the model
console.log(user.validationError);