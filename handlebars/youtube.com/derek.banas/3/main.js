// Topic: HTML escaping

/* 
	Handlebars HTML-escapes values returned by a {{expression}} and does not escape values 
	returned by a {{{expression}}}
*/
var data = {
	html_not_escaped: "<i>html_not_escaped</i>",
	html_escaped: "<html_escaped>"
},
template = Handlebars.compile($("#info-template").html());

$("#data").html(template(data));