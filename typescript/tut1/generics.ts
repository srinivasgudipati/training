function echo<T>(arg: T): T {
  return arg;
}

var myStr = echo("1");

class Person {
  firstname: string;
  lastname: string;

  constructor(firstname: string, lastname: string) {
    this.firstname = firstname;
    this.lastname = lastname;
  }

  getFullName() {
    return this.firstname + ' ' + this.lastname;
  }
}

class Admin extends Person {}

class Manager extends Person {}

let admin = new Admin('a', 'a');

let manager = new Manager('m', 'm');

function personEcho<T extends Person>(person: T): T {
  return person;
}

var foo = personEcho(admin);
var bar = personEcho(manager);

