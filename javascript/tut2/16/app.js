function greet(firstname, lastname, language) {

	language = language || 'en'

	if(!arguments.length) {
		console.log('Missing arguments!');
		console.log("-----------------------");
		return;
	}
	console.log(firstname);
	console.log(lastname);
	console.log(language);
	console.log(arguments);
	console.log("-----------------------");
}

greet();
greet('John');
greet('John', 'Doe');
greet('John', 'Doe', 'es');