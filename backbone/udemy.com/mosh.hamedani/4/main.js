// Topic: Models Inheritance

var Animal = Backbone.Model.extend({
	walk: function() {
		console.log("Animal is walking");
	}
});

var Dog = Animal.extend({
	walk: function() {
		Animal.prototype.walk.call(this);
		console.log("Dog is walking");
	}
});

var animal = new Animal();
var dog = new Dog();
dog.walk();