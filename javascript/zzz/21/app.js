// Topic: Good inheritance

function Fee() {
	
}

function Fi() {
	
}

Fi.prototype = Object.create(Fee.prototype);

Fee.prototype.prop1 = 'prop1';
Fi.prototype.prop2 = 'prop2';

var fi = new Fi();
var fee = new Fee();

console.log(fi.prop1);
console.log(fi.prop2);

console.log(fee.prop1);
console.log(fee.prop2);