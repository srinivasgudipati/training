// Topic: Collections: reduce

var arr = [1, 2, 3];

// curr holds the current value in the array
// prev holds the value from the previous iteration
console.log(arr.reduce(function (prev, curr) {
	return prev + curr;
}, 0));

console.log(_.reduce(arr, function (prev, curr) {
	return prev + curr;
}, 0));