// Topic: 'with' block helper

var template = Handlebars.compile($("#the-template").html());

Handlebars.registerHelper("with", function(context, options) {
	return options.fn(context);
});

$("#data").html(template({
	title: "Harry Potter - Sorcerrer Stones",
	info: {
		author: "J.K.Rowling",
		genre: "Fantasy"
	}
}));