function buildFunctions() {
	var arr = [];
	for (var i=0; i<3; i++) {
		arr.push(function () {
			console.log(i);
		});
	}
	return arr;
}

var fs = buildFunctions();

fs[0]();
fs[1]();
fs[2]();


function buildFunctions1() {
	var arr = [];
	for (var i=0; i<3; i++) {
		arr.push(function(j) {
			return function() {
				console.log(j);
			}
		}(i));
	}
	return arr;
}

var fs1 = buildFunctions1();

fs1[0]();
fs1[1]();
fs1[2]();