// Topic: 'if' block helper

var template = Handlebars.compile($("#the-template").html());

Handlebars.registerHelper("if", function (data, options) {
	if (data === "on") {
		return options.fn(this);
	} else {
		return options.inverse(this);
	}
});

$("#data").html(template());