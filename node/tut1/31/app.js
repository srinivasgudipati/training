var http = require('http');
var fs = require('fs');

var server = http.createServer(function(req, res) {

	res.writeHead(200, {'Content-Type': 'text/html'});
	var html = fs.readFileSync(__dirname + '/index.htm', 'utf8');
	html = html.replace('{Message}', 'Hello World!');
	res.end(html);

}).listen(1337, '127.0.0.1');