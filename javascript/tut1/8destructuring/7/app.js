// Topic: Destructuring

function double([ number, ...rest ]) {
	if(rest.length) {
		return [ number * 2, ...double(rest) ];
	} else {
		return [ number * 2 ];
	}
}

console.log(double([ 1, 2, 3 ]));

/*
double(1, [ 2, 3 ]) {
	return [ 2, ...double([ 2, 3 ]) ];
	double(2, [ 3 ]) {
		return [ 4, ...double([ 3 ]) ];
		double([ 3 ]) {
			return [6];
		}
		return [ 4, 6];
	}
	return [ 2, 4, 6 ];
}
*/