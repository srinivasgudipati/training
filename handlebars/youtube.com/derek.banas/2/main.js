// Topic: Arrays

var data = {
	songs: [
		{
			title: "Hello",
			artist: "Adele",
			genre: "Pop"
		},
		{
			title: "Thriller",
			artist: "Michael Jackson",
			genre: "Pop"
		},
		{
			title: "Home",
			artist: "Michael Buble",
			genre: "Country"
		}				
	]
},
template = Handlebars.compile($("#song-template").html());

$("#data").html(template(data));