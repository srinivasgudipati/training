var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person() {
    }
    Person.prototype.greet = function () {
        console.log('Hello there!');
    };
    return Person;
}());
var Programmer = /** @class */ (function (_super) {
    __extends(Programmer, _super);
    function Programmer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Programmer.prototype.greet = function () {
        console.log("Hello World!");
    };
    Programmer.prototype.greet1 = function () {
        _super.prototype.greet.call(this); // to refer to the public and protected methods of the inherited class
    };
    return Programmer;
}(Person));
var aProgrammer = new Programmer();
aProgrammer.greet();
aProgrammer.greet1();
var aProgrammer1 = new Programmer(); //polymorphism
aProgrammer1.greet(); //aProgrammer instance has acess only to the methods of Person class but executes the method defined in Programmer as it its instance
