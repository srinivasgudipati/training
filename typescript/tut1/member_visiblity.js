var Person = /** @class */ (function () {
    function Person(firstname, lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
    Person.prototype.getFirstName = function () {
        return this.firstname;
    };
    Person.prototype.setFirstName = function (firstname) {
        this.firstname = firstname;
    };
    Person.prototype.getLastName = function () {
        return this.lastname;
    };
    Person.prototype.setLastName = function (lastname) {
        this.lastname = lastname;
    };
    Person.prototype.getFullName = function () {
        return this.firstname + ' ' + this.lastname;
    };
    return Person;
}());
var aPerson = new Person("Srinivas", "Gudipati");
console.log(aPerson.getFirstName());
aPerson.setFirstName('Sanjay');
console.log(aPerson.getFirstName());
