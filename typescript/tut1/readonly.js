var Person = /** @class */ (function () {
    function Person(name) {
        this.name = 'Srinivas'; // value can be set for a readonly attribute during declaration
        this.name = name; // value can be set for a readonly attribute inside a constructor
    }
    return Person;
}());
var Animal = /** @class */ (function () {
    function Animal(name) {
        this.name = name;
    }
    return Animal;
}());
var aPerson = new Person('Sanjay');
console.log(aPerson.name);
var aAnimal = new Animal('Lion');
console.log(aAnimal.name);
