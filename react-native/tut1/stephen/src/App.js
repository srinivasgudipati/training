import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

class App extends Component {
  componentWillMount() {
    const config = {
			apiKey: "AIzaSyA9JjwRT042d2m-YdnpMlfG9XscA16gky0",
			authDomain: "stephen-2ba70.firebaseapp.com",
			databaseURL: "https://stephen-2ba70.firebaseio.com",
			projectId: "stephen-2ba70",
			storageBucket: "stephen-2ba70.appspot.com",
			messagingSenderId: "98188250210"
    };

    firebase.initializeApp(config);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
