// Topic: callback function

var friends = ['Parag', 'Sai', 'Ravinder', 'Gopi'];

// Method 1 - Using for loop
for (var i = 0; i < friends.length; i++) {
	console.log(i + 1 + '.', friends[i]);
}
console.log('=====================================');

// Method 2 - Using callback function
function each(arr, callback)  {
	for (var i = 0; i < arr.length; i++) {
		callback(arr[i], i);
	}
}

each(friends, function(friend, index) {
	console.log(index + 1 + '.', friend);
});
console.log('=====================================');

// Method 3 - Using in-built forEach function of array
friends.forEach(function (friend, index) {
	console.log(index + 1 + '.', friend);
});
console.log('=====================================');