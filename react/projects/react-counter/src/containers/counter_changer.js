import React, { Component } from 'react';
import { connect } from 'react-redux';
import { incrementCount, decrementCount } from '../actions/index';
import { bindActionCreators } from 'redux';

class CounterChanger extends Component {
	render() {
		return (
			<div className="counter-changer">
				<button
					type="button"
					className="btn btn-primary btn-circle"
					onClick={() => this.props.incrementCount() }>
					<i className="glyphicon glyphicon-plus"></i>
				</button>
				<button
					type="button"
					className="btn btn-primary btn-circle"
					onClick={ () => { this.props.decrementCount() } }>
					<i className="glyphicon glyphicon-minus"></i>
				</button>
			</div>
		);
	}
}

// Anything returned from this function will end up as props on the BookList container
function mapDispatchToProps(dispatch) {
	// Whenever incrementCount is called, the result should be passed to all our reducers
	return bindActionCreators(
		{
			incrementCount: incrementCount,
			decrementCount: decrementCount,
		}, dispatch
	);
}

// Promote CounterChanger from a component to a container. It needs to know about this new dispatch method, incrementCount. Make it available as a prop.
export default connect(null, mapDispatchToProps)(CounterChanger);