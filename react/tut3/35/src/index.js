import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {PhotoStory, VideoStory} from './stories';

const components = {
	photo: PhotoStory,
	video: VideoStory
};

function Story(props) {
	const SpecificStory = components[props.storyType];
	return <SpecificStory story={props.story} />
}

ReactDOM.render(
	<Story storyType="photo" story={"Here is the story"}/>,
	document.getElementById('container')
);