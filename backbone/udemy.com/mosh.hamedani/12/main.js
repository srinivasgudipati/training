// Topic: Views: Handling Model events

var Song = Backbone.Model.extend({
	defaults: {
		listeners: 0
	}
});

var song = new Song({ title: 'Song 1' });

var SongView = Backbone.View.extend({
	initialize: function() {
		this.model.on("change", this.render, this); // All backbone models publish a change event whenever one of its attributes is changed
	},
	render: function() {
		this.$el.html(this.model.get("title") + " - Listeners: " + this.model.get("listeners"));
	}
});

var songView = new SongView({ el: "#container", model: song });
songView.render();