// Topic: Array helpers: forEach

// Create array of numbers
var numbers = [ 1, 2, 3, 4, 5 ];

// Create variable to hold the sum
var sum = 0;

function adder(number) {
	sum += number;
}

// Loop over the array and add the numbers
numbers.forEach(adder);

// Print the sum
console.log(sum);