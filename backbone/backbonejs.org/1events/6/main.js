// Topic: Events: once

var person = {
	name: "Name 1",
	changeName: function (name) {
		this.name = name;
		this.trigger("change:name", name);
	}
};

_.extend(person, Backbone.Events);

// Calling "once" instead of "on" causes the bound callback to fire only once before being removed
person.once("change:name", function (name) {
	console.log("Changed name to", name);
});

person.changeName("Name 2");
person.changeName("Name 3");