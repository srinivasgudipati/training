function waitThreeSeconds() {
	var ms = new Date().getTime() + 3000;
	while (new Date() < ms ) {}		
	console.log('finished function');
}

function clickHandler() {
	console.log('click event!');
}

document.addEventListener('click', clickHandler);

waitThreeSeconds();
console.log('finished execution');