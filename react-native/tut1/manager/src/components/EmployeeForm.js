import React, { Component } from 'react';
import { View, Text, Picker } from 'react-native';
import { connect } from 'react-redux';

import { CardSection, Input } from './common';
import { employeeChanged } from '../actions';

class EmployeeForm extends Component {
	handleNameChange = this.handleNameChange.bind(this);
	handlePhoneChange = this.handlePhoneChange.bind(this);
	handleShiftChange = this.handleShiftChange.bind(this);
	
	handleNameChange(value) {
		this.props.employeeChanged({ prop: 'name', value });
	}

	handlePhoneChange(value) {
		this.props.employeeChanged({ prop: 'phone', value });
	}

	handleShiftChange(value) {
		this.props.employeeChanged({ prop: 'shift', value });
	}

	render() {
		return (
			<View>
				<CardSection>
					<Input
						label='Name'
						placeholder='Jane'
						value={this.props.name}
						onChangeText={this.handleNameChange}
					/>
				</CardSection>
				<CardSection>
					<Input
						label='Phone'
						placeholder='555-555-5555'
						value={this.props.phone}
						onChangeText={this.handlePhoneChange}
					/>
				</CardSection>
				<CardSection style={{ flexDirection: 'column' }}>
					<Text style={styles.pickerLabelStyle}>Shift</Text>
					<Picker
						style={{ flex: 1 }}
						selectedValue={this.props.shift}
						onValueChange={this.handleShiftChange}
					>
						<Picker.Item label='Monday' value='Monday' />
						<Picker.Item label='Tuesday' value='Tuesday' />
						<Picker.Item label='Wednesday' value='Wednesday' />
						<Picker.Item label='Thursday' value='Thursday' />
						<Picker.Item label='Friday' value='Friday' />
						<Picker.Item label='Saturday' value='Saturday' />
						<Picker.Item label='Sunday' value='Sunday' />
					</Picker>
				</CardSection>
			</View>			
		);
	}
}

const styles = {
	pickerLabelStyle: {
		fontSize: 18,
		paddingLeft: 20
	}
};

const mapStateToProps = state => {
	const { name, phone, shift } = state.employeeForm;

	return { name, phone, shift };
};

export default connect(mapStateToProps, { employeeChanged })(EmployeeForm);
