import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';

import { Header, Spinner, Button, Card, CardSection } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {
	state = { loggedIn: null };

	componentWillMount() {
		const config = {
			apiKey: 'AIzaSyB_2I1EVqUeLu3p0hDeTQ_pXq-88pCyE-s',
			authDomain: 'auth-517bc.firebaseapp.com',
			databaseURL: 'https://auth-517bc.firebaseio.com',
			projectId: 'auth-517bc',
			storageBucket: 'auth-517bc.appspot.com',
			messagingSenderId: '1020873535250'
		};
		firebase.initializeApp(config);

		firebase.auth().onAuthStateChanged(user => {
			if (user) {
				this.setState({ loggedIn: true });
			} else {
				this.setState({ loggedIn: false });
			}
		});
	}

	renderContent() {
		switch (this.state.loggedIn) {
			case true:
				return (
					<Card>
						<CardSection>
							<Button onPress={() => firebase.auth().signOut()}>
								Log out
							</Button>
						</CardSection>
					</Card>
				);
			case false:
				return <LoginForm />;
			default:
				return <Spinner />;
		}
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<Header headerText='Authentication' />
				{this.renderContent()}
			</View>
		);
	}
}

export default App;
