// Topic: callback function

var allUserData = [];

// prints to console
function logStuff(userData) {
	if(typeof userData === 'object') {
		for (var i in userData) {
			console.log(i+':', userData[i]);
		}
	}
}

function getInput(options, callback) {
	allUserData.push(options);
	callback(options);
}

getInput({ name: 'Srinivas', speciality: 'JavaScript' }, logStuff);
console.log(allUserData);