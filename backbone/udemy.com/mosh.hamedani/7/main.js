var Vehicle = Backbone.Model.extend({
	idAttribute: "registrationNumber",

	urlRoot: "/api/vehicles",

	start: function() {
		console.log("Vehicle started");
	},

	validate: function(attrs) {
		if(!attrs.registrationNumber) {
			return "Registration Number cannot be null";
		}
	}
});

var Car = Vehicle.extend({
	start: function() {
		console.log("Car with registration number " + this.get("registrationNumber") + " started.");
	}
});

var Vehicles = Backbone.Collection.extend({
	model: Vehicle
});

var vehicles = new Vehicles();
vehicles.add(new Car({
	registrationNumber: "XLI887",
	color: "Blue"
}));
vehicles.add(new Car({
	registrationNumber: "ZNP123",
	color: "Blue"
}));
vehicles.add(new Car({
	registrationNumber: "XUV456",
	color: "Gray"
}));

console.log(vehicles.where({color: "Blue"}));
console.log(vehicles.where({registrationNumber: "XLI887"}));
vehicles.remove(vehicles.where({registrationNumber: "XLI887"}));
console.log(vehicles.toJSON());
vehicles.each(function(vehicle) {
	console.log(vehicle);
});