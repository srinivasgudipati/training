// Topic: Templating using handlebars

// JS template
var myInfo = "<p>My name is {{ name }} and I live at {{ street }} in {{ city }}, {{ state }}</p>"

// Compile above template into function that can be evaluated for rendering
var template = Handlebars.compile(myInfo);

// Pass data and evaluate the above function
var data = template({
	name: "Srinivas",
	street: "123 Main St",
	city: "San Francisco",
	state: "CA"
});

// Render the evaluated template on the DOM
$("#data").html(data);