export function incrementCount() {
	// incrementCount is an ActionCreator, it needs to return an action, an object with a type property.
	return {
		type: 'INCREMENT'
	};
}

export function decrementCount() {
	// decrementCount is an ActionCreator, it needs to return an action, an object with a type property.
	return {
		type: 'DECREMENT'
	};
}