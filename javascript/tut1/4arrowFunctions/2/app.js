// Topic: Fat arrows

var numbers = [1, 2, 3];

var double = numbers.map(number => number * 2);

console.log(double);