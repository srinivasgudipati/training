// Topic: Views: Handling Collection events

var Song = Backbone.Model.extend();

var Songs = Backbone.Collection.extend({
	model: Song
});

var songs = new Songs([
	new Song({ id: 1, title: 'Song 1' }),
	new Song({ id: 2, title: 'Song 2' }),
	new Song({ id: 3, title: 'Song 3' })
]);

var SongView = Backbone.View.extend({
	tagName: 'li',
	render: function() {
		this.$el.html(this.model.get("title"));
		this.$el.attr('id', this.model.id);

		return this;
	}
});

var SongsView = Backbone.View.extend({
	initialize: function() {
		this.model.on('add', this.onSongAdded, this);
		this.model.on('remove', this.onSongRemoved, this);
	},
	onSongAdded: function(song) {
		this.$el.append(new SongView({ model: song }).render().$el);
	},
	onSongRemoved: function(song) {
		//this.$el.find("li#" + song.id).remove();
		this.$("li#" + song.id).remove();
	},	
	render: function() {
		var self = this;
		this.model.each(function(song) {
			self.$el.append(new SongView({ model: song }).render().$el);
		});
	}
});

var songsView = new SongsView({ el: "#songs", model: songs });
songsView.render();