class Person {
  constructor(
    protected firstname: string, // can be accessed within this class and any class that extends this class
    public lastname: string, // can be accessed by any one
    private age: number // can be accessed only within this class
  ) {}

  intro() {
    console.log(`My name is ${this.firstname} ${this.lastname}. I am ${this.age} years old`);
  }
}

class Programmer extends Person {
  intro() {
    console.log(`My name is ${this.firstname} ${this.lastname}`);
  }
}

var aPerson = new Person("Srinivas", "Gudipati", 43);
var aProgrammer = new Programmer("Sanjay", "Gudipati", 6);

aPerson.intro();
aProgrammer.intro();