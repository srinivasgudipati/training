// Topic: Helpers: Pass options as name/value pairs to helper functions

var template = Handlebars.compile('{{changeColor "This is my text" color="green"}}');

// Registers helpers accessible by any template in the env
Handlebars.registerHelper('changeColor', function (text, options) {
	// HTML escapes the passed string, making it safe for rendering as text within HTML content
	text = Handlebars.Utils.escapeExpression(text);
	var theLink = '';
	if (options.hash.color === 'red') {
		// SafeString prevents the string from being escaped when the template is rendered
		return new Handlebars.SafeString('<span class="redText">'+ text +'</span>');
	} else if (options.hash.color === 'blue') {
		return new Handlebars.SafeString('<span class="blueText">'+ text +'</span>');
	} else if (options.hash.color === 'green') {
		return new Handlebars.SafeString('<span class="greenText">'+ text +'</span>');
	}
});

$("#data").html(template());