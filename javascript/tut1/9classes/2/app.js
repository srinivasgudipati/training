// Topic: class

class Monster {
  constructor(options) {
    this.health = 100;
    this.name = options.name;
  }
}

class Snake extends Monster {
  constructor(options) {
      super(options);
  }
  
  bite(snake) {
      snake.health = snake.health - 10;
  }
}

snake = new Snake({ name: 'Slither' });
snake.bite(snake);

console.log(snake);