import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableWithoutFeedback,
	LayoutAnimation
} from 'react-native';
import { connect } from 'react-redux';

import { CardSection } from './common';
import * as actions from '../actions';

class LibraryListItem extends Component {
	componentWillUpdate() {
		LayoutAnimation.easeInEaseOut();
	}

	renderDescription() {
		const { library, expanded } = this.props;

		if (expanded) {
			return (
				<CardSection>
					<Text>{library.description}</Text>
				</CardSection>
			);
		}
	}

	render() {
		console.log('LibraryListItem:render');
		console.log('LibraryListItem:render::props', this.props);
		const { id, title } = this.props.library;

		return (
			<TouchableWithoutFeedback
				onPress={() => this.props.selectLibrary(id)}
			>
				<View>
					<CardSection>
						<Text style={styles.titleStyle}>
							{title}
						</Text>
					</CardSection>
					{this.renderDescription()}
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

const styles = {
	titleStyle: {
		paddingLeft: 15,
		fontSize: 18
	}
};

const mapStateToProps = (state, ownProps) => {
	console.log('LibraryListItem:mapStateToProps');
	console.log('LibraryListItem:mapStateToProps::state', state);
	const expanded = state.selectedLibraryId === ownProps.library.id;
	
	return { expanded };
};

console.log('LibraryListItem::actions', actions);
export default connect(mapStateToProps, actions)(LibraryListItem);
