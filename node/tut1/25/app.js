function greet(callback) {
	console.log('Hello');
	var data = {
		name: 'John Doe'
	};
	callback(data);
}

greet(function(data) {
	console.log('Callback is invoked');
	console.log(data);
});

greet(function(data) {
	console.log('Another callback is invoked');
	console.log(data.name);
});