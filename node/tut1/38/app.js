var express = require('express');
var app = express();

var port = process.env.PORT || 1337;

app.use('/assets', express.static(__dirname + '/public'));

app.use('/', function(req, res, next) {
	console.log('Request URL is ' + req.url);
	next();
});

app.get('/', function(req, res) {
	res.send('<html><head><link href=assets/styles.css type=text/css rel=stylesheet></head><body><b>Hello World</b></body></html>');
});

app.listen(port);