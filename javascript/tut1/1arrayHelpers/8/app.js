// Topic: Array helpers: find

function Car(model) {
	this.model = model;
}

var cars = [
	new Car('Buick'),
	new Car('Focus'),
	new Car('Infinity')
];

var car;

car = cars.find(function(car) {
	return car.model === 'Buick';
});

console.log(car);