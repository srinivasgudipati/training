define([
	'jquery',
	'underscore',
	'backbone',
	'models/vehicle'], function($, _, Backbone, Vehicle) {
		var NewVehicleView = Backbone.View.extend({

			initialize: function(options) {
				this.bus = options.bus;
			},

			events: {
				"click .add": "onClick"
			},

			onClick: function() {
				var input = this.$el.find(".registration-number");
				this.bus.trigger("newVehicle", new Vehicle({ registrationNumber: input.val()}));

				input.val("");
			},

			render: function() {
				var template = _.template($("#newVehicleTemplate").html());
				this.$el.html(template());

				return this;
			}
		});

		return NewVehicleView;
});