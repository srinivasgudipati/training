// Topic: Rest operator

//ES5
function add(a, b, c, d) {
	var numbers = [a, b, c, d];
	return numbers.reduce((sum, number) => {
		return sum + number;
	}, 0);
}

console.log(add(1, 2 ,3, 4));

//ES6
function add1(...numbers) {
	return numbers.reduce((sum, number) => {
		return sum + number;
	}, 0);
}

console.log(add1(1, 2, 3, 4, 5, 6 ));
