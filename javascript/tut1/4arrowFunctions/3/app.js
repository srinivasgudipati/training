// Topic: Fat arrows

// ES5 - Using that
const team = {
	members: ['John', 'Jane'],
	name: 'Odd Squad',
	getSummary: function() {
		var self = this;
		return this.members.map(function(member) {
			return member + ' belongs to ' + self.name;
		});
	}
}

console.log(team.getSummary());

// ES5 - Using bind
const team1 = {
	members: ['John', 'Jane'],
	name: 'Odd Squad',
	getSummary: function() {
		return this.members.map(function(member) {
			return member + ' belongs to ' + this.name;
		}.bind(this));
	}
}

console.log(team1.getSummary());

// ES6 - Using =>
const team2 = {
	members: ['John', 'Jane'],
	name: 'Odd Squad',
	getSummary: function() {
		return this.members.map(member => {
			return `${member} belongs to ${this.name}`;
		});
	}
}

console.log(team2.getSummary());
