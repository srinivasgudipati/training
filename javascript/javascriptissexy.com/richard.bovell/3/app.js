// Topic: callback function

var clientData = {
	id: 123,
	fullName: 'Not Set',
	setUserName: function (firstName, lastName) {
		this.fullName = firstName + ' ' + lastName;
	}
};

function getUserInput(firstName, lastName, callback, callbackObj) {
	callback.apply(callbackObj, [firstName, lastName]);
}

getUserInput('Srinivas', 'Gudipati', clientData.setUserName, clientData);

console.log(clientData.fullName);