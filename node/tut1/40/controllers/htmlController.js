var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })

module.exports = function(app) {
	app.get('/', function(req, res) {
		res.render('index');
	});

	app.get('/person/:id', function(req, res) {
		res.render('person', { id: req.params.id, qstr: req.query.qstr });
	});

	app.post('/person', urlencodedParser, function(req, res) {
		console.log(req.body.firstname);
		console.log(req.body.lastname);
		res.send('Thank you!');
	});
}