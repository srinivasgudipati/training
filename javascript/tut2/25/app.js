function makeGreetings(language) {
	return function(firstname, lastname) {
		if (language === 'en') {
			console.log('Hello ' + firstname + ' ' + lastname);
		} else if (language === 'es') {
			console.log('Hola ' + firstname + ' ' + lastname);
		}
	}
}

var greetEnglish = makeGreetings('en');
var greetSpanish = makeGreetings('es');

greetEnglish('John', 'Doe');
greetSpanish('John', 'Doe');