// Topic: Scope, variables

var TAX_RATE = 0.08;
var PHONE_PRICE = 199.99;
var ACCESSORY_PRICE = 9.99;
var SPENDING_THRESHOLD = 350;

var bank_balance = 350.91;
var amount = 0;

function calculateTax(amount) {
	return amount * TAX_RATE;
}

function formatAmount(amount) {
	return "$" + amount.toFixed(2);
}

// keep buying phones till you run out of money
while (amount < bank_balance) {
	// buy a new phone
	amount = amount + PHONE_PRICE;

	// can we afford accessory?
	if(amount < SPENDING_THRESHOLD) {
		amount = amount + ACCESSORY_PRICE;
	}
}

// dont forget to pay the government
amount = amount + calculateTax(amount);

console.log("Your purchase: ", formatAmount(amount));

// can you actually afford this purchase?
if (amount > bank_balance) {
	console.log("Sorry but you can't afford");
} else {
	console.log("Congratulations on your purchases!");
}