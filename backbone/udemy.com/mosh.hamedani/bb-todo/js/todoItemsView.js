var TodoItemsView = Backbone.View.extend({
	tagName: "ul",

	id: "todoItems",

	initialize: function(options) {
		if(!(options && options.collection)) {
			throw new Error("Collection is not specified");
		}

		this.collection.on("add", this.onAddTodoItem, this);
		this.collection.on("remove", this.onRemoveTodoItem, this);
	},

	onAddTodoItem: function(todoItem) {
		this.$el.append(new TodoItemView({ model: todoItem }).render().$el);
	},

	onRemoveTodoItem: function(todoItem) {
		this.$el.find("li#" + todoItem.get("id")).remove();
	},

	events: {
		"click #add": "onClickAdd",
		"keypress #newTodoItem": "onKeyPress"
	},

	onKeyPress: function(e) {
		if(e.keyCode === 13 ) {
			this.onClickAdd();
		}
	},

	onClickAdd: function() {
		var $description = this.$("#newTodoItem");
		if($description.val()) {
			this.collection.add(new TodoItem({ description: $description.val() }));
			$description.val("");
		}
	},

	render: function() {
		var self = this;

		this.$el.append("<input id='newTodoItem' autofocus type='text'>");
		this.$el.append("<button id='add' type='submit'>Add</button>");

		this.collection.each(function(todoItem) {
			self.$el.append(new TodoItemView({ model: todoItem }).render().$el);
		});

		return this;
	}
});