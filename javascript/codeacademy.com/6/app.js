var stack = [];

function countDown(int) {
  stack.push(int);
  if (int === 1) {	
    return 1;
  }
    return countDown(int - 1);
}

function multiplyEach() {
  int = stack.pop();
  
  x = stack.length;

  if (x === 0) {
    return int;
  }

  else {
	stack[x - 1] = int * stack[x - 1];
	return multiplyEach();
  }
}

countDown(4);

console.log(multiplyEach());

/*
Control flow
countdown(4);
stack = [4, 3, 2, 1];

multiplyEach() {
	int = 1;
	stack = [4, 3, 2];
	x = 3;
	stack[2] = 1 * stack[2];
	stack = [4, 3, 2];
	return multiplyEach();
	multiplyEach() {
		int = 2;
		stack = [4, 3];
		x = 2;
		stack[1] = 2 * stack[1];
		stack = [4, 6];
		return multiplyEach();
		multiplyEach() {
			int = 6;
			stack = [4];
			x = 1;
			stack[0] = 6 * stack[0];
			stack = [24];
			return multiplyEach();
			multiplyEach() {
				int = 24;
				stack = [];
				x = 0;
				return 24;
			}
			return 24;
		}
		return 24;
	};
	return 24;
};
*/