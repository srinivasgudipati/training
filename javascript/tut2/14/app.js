function a() {
	console.log(this);
	this.newvariable = "hello";
}

var b = function () {
	console.log(this);
}

a();

console.log(newvariable);

b();

var c = {
	name: 'object name is c',
	log: function () {
		this.name = 'object c is modified!';
		console.log(this);

		var setname = function (name) {
			this.name = name;
		};
		setname('object c is modified again!');
		console.log(this);
	}
};

c.log();

var d = {
	name: 'object name is d',
	log: function () {
		var that = this;
		that.name = 'object d is modified!';
		console.log(that);

		var setname = function (name) {
			that.name = name;
		};
		setname('object d is modified again!');
		console.log(that);
	}
};

d.log();