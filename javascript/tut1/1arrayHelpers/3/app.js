// Topic: Array helpers: map

var numbers = [1, 2, 3];

var numbers1 = [],
	numbers2 = [],
	numbers3 = [];

// using for loop
for (var i = 0; i < numbers.length; i++) {
	numbers1.push(numbers[i] * 2);
}

console.log(numbers1);

// using forEach helper
numbers.forEach(function(number) {
	numbers2.push(number * 2);
});

console.log(numbers2);

// using map helper
numbers3 = numbers.map(function(number) {
	return number * 2;
});

console.log(numbers3);