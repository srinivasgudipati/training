// Topic: prototype inhertance

// Method1: Directly setting object as prototype using __proto__ property
var a = {
	x: 10,
	calculate: function(z) {
		return this.x + this.y + z;
	}
};

var b = {
	y: 20,
	__proto__: a
}

console.log(b.calculate(30));

//Method2: Using constructor function
// A constructor function which may create objects by specified pattern
function Foo(y) {
	// Each object created by using this pattern will have their own "y" property
	this.y = y;
}

// Also "Foo.prototype" stores reference to the prototype of newly created objects, so we may use it to define shared/inherited properties or methods.

// Inherited property "x"
Foo.prototype.x = 10;

// Inherited method "calculate"
Foo.prototype.calculate = function(z) {
	return this.x + this.y + z;
}

// Create objec c using pattern "Foo"
var c = new Foo(20);

// Call the inherited method
console.log(c.calculate(30));

console.log('c.__proto__ === Foo.prototype is', c.__proto__ === Foo.prototype);

// Also "Foo.prototype" automatically creates a special property "constructor", which is a reference to the constructor function itself; instances "c" may find it via delegation and use to check their constructor
console.log('c.constructor === Foo is', c.constructor === Foo);