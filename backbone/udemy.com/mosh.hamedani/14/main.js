// Topic: Views: Templates

var Song = Backbone.Model.extend({});

var SongView = Backbone.View.extend({
	render: function() {
		//this.$el.html(this.model.get("title") + '<button>Listen</button');
		var template = _.template($("#songTemplate").html());
		this.$el.html(template(this.model.toJSON()));
	}
});

var songView = new SongView({ el: "#container", model: new Song({ title: "Song 1", views: 10 }) });
songView.render();