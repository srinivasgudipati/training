// Topic: Array helpers: every

function Field(value) {
	this.value = value;
}

Field.prototype.validate = function() {
	 return this.value.length > 0;
}

var username = new Field("user1");
var password = new Field("pass1");
var gender = new Field("Male");

var fields = [username, password, gender];
var isFormValid = fields.every(function(field) {
	return field.validate();
});

console.log(isFormValid);