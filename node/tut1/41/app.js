var express = require('express');
var app = express();
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://test:test@ds143241.mlab.com:43241/addressdb', { config: { autoIndex: false } });

var personSchema = new Schema({
	firstname: String,
	lastname: String,
	address: String
});

var Person = mongoose.model('Person', personSchema);

var john = new Person({
	firstname: 'John',
	lastname: 'Doe',
	address: '555 Westbrook Ave.'
});

john.save(function(err) {
	if (err) throw err;
	console.log('Person saved');
});

var jane = new Person({
	firstname: 'Jane',
	lastname: 'Doe',
	address: '555 Westbrook Ave.'
});

jane.save(function(err) {
	if (err) throw err;
	console.log('Person saved');
});

app.use('/', function(req, res, next) {
	Person.find({}, function(err, data) {
		console.log(data);
	});
	next();
});

app.get('/', function(req, res) {
	res.send('Hello World');
});

var port = process.env.PORT || 1337;

app.listen(port);