import React, { Component } from 'react';
import firebase from 'firebase';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';

import reducers from './reducers';
import Router from './Router';

class App extends Component {
	componentWillMount() {
		const config = {
			apiKey: 'AIzaSyATIl-9B5_ABFoZ-JpU8LDmH33K-sU2GH4',
			authDomain: 'manager-8628d.firebaseapp.com',
			databaseURL: 'https://manager-8628d.firebaseio.com',
			projectId: 'manager-8628d',
			storageBucket: 'manager-8628d.appspot.com',
			messagingSenderId: '220322132103'
		};

		firebase.initializeApp(config);		
	}

	render() {
		const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

		return (
			<Provider store={store}>
				<Router />
			</Provider>
		);
	}

}
export default App;
