// Topic: Fat arrows

// ES5
const add1 = function(a, b) {
	return a + b;
}

console.log(add1(2, 3));

// ES6
const add2 = (a, b) => {
	return a + b;
}

console.log(add2(2, 3));

// ES6 - Implicit return
const add3 = (a, b) => a + b;

console.log(add2(2, 3));

// ES6 - No parenthesis needed in case of single input parameter
const square = a => a * a;

console.log(square(2));

// ES6 - Parenthesis needed in case of no input parameters
const sayHello = () => 'Hello';

console.log(sayHello());