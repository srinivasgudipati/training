// Topic: Utiltity: template

var artists = ['Pharel Williams', 'Rolling Stones', 'Michael Jackson'],
		artistTemplate = _.template(
			'<% _.each(artists, function (artist) { %>' +
			'<li><%= artist %></li>' +
			'<% }); %>'
		),
		content = artistTemplate({
			artists: _.filter(artists, function (artist) {
				return artist === 'Michael Jackson';
			})
		});

var container = document.createElement('ol');
container.innerHTML = content;
document.body.appendChild(container);