// Topic: recursive functions

function power(base, exponent) {
	if(exponent == 0) {
		return 1;
	}
	return base * power(base, exponent - 1);
}

console.log(power(2, 3));

/*
	power(2, 3) {
		return 2 * power(2, 2); // 2 * 4 = 8
		power(2, 2) {
			return 2 * power(2, 1); // 2 * 2 = 4
			power(2, 1) {
				return 2 * power(2, 0); // 2 * 1 = 2
				power(2, 0) {
					return 1;
				}
			}
		}
	}
*/