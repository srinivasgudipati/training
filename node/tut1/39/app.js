var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// create application/json parser
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

var port = process.env.PORT || 1337;

app.set('view engine', 'ejs');

app.use('/assets', express.static(__dirname + '/public'));

app.use('/', function(req, res, next) {
	console.log('Request URL is ', req.url);
	next();
});

app.get('/', function(req, res) {
	res.render('index');
});

app.get('/person/:id', function(req, res) {
	res.render('person', { id: req.params.id, qstr: req.query.qstr });
});

app.post('/personjson', jsonParser, function(req, res) {
	console.log(req.body.firstname);
	console.log(req.body.lastname);
	res.send('Thank you for submitting json!');
});

app.post('/person', urlencodedParser, function(req, res) {
	console.log(req.body.firstname);
	console.log(req.body.lastname);
	res.send('Thank you!');
});

app.get('/api', function(req, res) {
	res.json({ firstname: 'John', lastname: 'Doe' });
});

app.listen(port);