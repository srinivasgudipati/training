/* Copy contents of an array to a different array and multiply them by 2 */

function mapForEach(arr, fn) {

	var newArr = [];
	for (var i=0; i < arr.length; i++) {
		newArr.push(fn(arr[i]));
	}
	return newArr;
}

var arr1 = [1, 2, 3];
console.log(arr1);

var arr2 = mapForEach(arr1, function(item) {
	return item * 2;
});

console.log(arr2);

var arr3 = mapForEach(arr1, function(item) {
	return item > 2;
});

console.log(arr3);

var checkPastLimit = function(limit, item) {
	return item > limit;
};

var arr4 = mapForEach(arr1, checkPastLimit.bind(this, 4));

console.log(arr4);

var checkPastLimit1 = function(limit) {
	return function(item) {
		return item > limit;
	};
};

var arr5 = mapForEach(arr1, checkPastLimit1(1));

console.log(arr5);

var checkPastLimit2 = function(limit) {
	return function(limit, item) {
		return item > limit;
	}.bind(this, limit);
};

var arr6 = mapForEach(arr1, checkPastLimit2(1));

console.log(arr6);