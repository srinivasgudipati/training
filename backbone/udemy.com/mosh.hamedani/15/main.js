// Topic: Views: overview

var Vehicle = Backbone.Model.extend();

var Vehicles = Backbone.Collection.extend({
	model: Vehicle
});

var vehicles = new Vehicles([
	new Vehicle({ registrationNumber: "XLI887", color: "Blue" }),
	new Vehicle({ registrationNumber: "ZNP123", color: "Blue" }),
	new Vehicle({ registrationNumber: "XUV456", color: "Gray" })
]);

var VehicleView = Backbone.View.extend({
	tagName: 'li',
	className: 'vehicle',
	events: {
		'click .delete': 'onDelete'
	},
	onDelete: function() {
		this.remove();
	},
	initialize: function() {
		this.model.on("change", this.render, this);
	},
	render: function() {
		var template = _.template($("#vehicleTemplate").html());
		this.$el.html(template(this.model.toJSON()));
		this.$el.attr("data-color", this.model.get("color"));
		this.$el.attr('id', this.model.get("registrationNumber"));
		return this;
	}
});

var VehiclesView = Backbone.View.extend({
	tagName: 'ul',
	initialize: function() {
		this.collection.on("add", this.onVehicleAdded, this);
		this.collection.on("remove", this.onVehicleRemoved, this);
	},
	onVehicleAdded: function(vehicle) {
		this.$el.append(new VehicleView({ model: vehicle }).render().$el);
	},
	onVehicleRemoved: function(vehicle) {
		this.$el.find("li#" + vehicle.get("registrationNumber")).remove();
	},
	render: function() {
		var self = this;
		this.collection.each(function(vehicle) {
			self.$el.append(new VehicleView({ model: vehicle }).render().$el);
		});

		return this;
	}
});

var vehiclesView = new VehiclesView({ collection: vehicles });
$("#container").append(vehiclesView.render().$el);