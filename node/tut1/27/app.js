var fs = require('fs');

var readable = fs.createReadStream(__dirname + '/greet1.txt', { encoding: 'utf8', highWaterMark: 32 * 1024 });
var writable = fs.createWriteStream(__dirname + '/greet2.txt');

readable.on('data', function(chunk) {
	writable.write(chunk);
});