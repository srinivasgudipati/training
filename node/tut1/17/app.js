var Emitter = require('events');
var util = require('util');

function Greetr() {
	Emitter.call(this);
	this.greeting = 'Hello';
}

util.inherits(Greetr, Emitter);

Greetr.prototype.greet = function(data) {
	console.log(this.greeting);
	this.emit('greet', data);
}

var greetr = new Greetr();
greetr.on('greet', function(data) {
	console.log('Someone said hello:', data);
});

greetr.greet('Srinivas');
