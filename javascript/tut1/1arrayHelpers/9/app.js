// Topic: Array helpers: find

var posts = [
	{ id: 1, title: 'New Post' },
	{ id: 2, title: 'Old Post' }
];

var comment = { postId: 1, content: 'Awesome post!' };

function postForComments(posts, comment) {
	return posts.find(function (post) {
		return post.id === comment.postId;
	});
}

console.log(postForComments(posts, comment));