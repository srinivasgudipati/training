define([
	'underscore',
	'backbone',
	'models/vehicle'], function(_, Backbone, Vehicle) {
		var Vehicles = Backbone.Collection.extend({
			model: Vehicle
		});

		return Vehicles;
});