var Venue = Backbone.Model.extend();

var Venues = Backbone.Collection.extend({
	model: Venue
});

var VenueView = Backbone.View.extend({
	tagName: "li",

	events: {
		"click delete": "onClick"
	},

	onClick: function() {
		this.bus.trigger("venueClicked", this.model);
	},

	initialize: function(options) {
		this.bus = options.bus;
	},

	render: function() {
		this.$el.html(this.model.get("name"));

		return this;
	}
});

var VenuesView = Backbone.View.extend({
	tagName: "ul",

	id: "venues",

	initialize: function(options) {
		this.bus = options.bus;
	},

	render: function() {
		var self = this;
		this.collection.each(function(venue) {
			var view = new VenueView({ model: venue, bus: self.bus });
			self.$el.append(view.render().$el);
		});

		return this;
	}
});

var MapView = Backbone.View.extend({
	el: "#map-container",

	initialize: function(options) {
		this.bus = options.bus;
		this.bus.on("venueClicked", this.onVenueClicked, this);
	},

	onVenueClicked: function(venue) {
		this.model = venue;
		this.render();
	},

	render: function() {
		if(this.model)
			this.$("#venue-name").html(this.model.get("name"));
	}
});

var bus = _.extend({}, Backbone.Events);

var venues = new Venues([
	new Venue({ name: "2562 Heatherlark Circle" }),
	new Venue({ name: "633 Palomino Drive" }),
	new Venue({ name: "646 Concord Place"})
]);

var venuesView = new VenuesView({ collection: venues, bus: bus });
$("#venues-container").html(venuesView.render().$el);

var mapView = new MapView({ bus: bus });
mapView.render()