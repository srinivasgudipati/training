define(function () {
	var cart = {};
	
	cart.items = [];
	cart.add = function (o) {
		this.items.push(o);
	};
	
	return cart;
});