var Person = /** @class */ (function () {
    function Person(firstname, lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
    Person.prototype.getFullName = function () {
        return this.firstname + ' ' + this.lastname;
    };
    return Person;
}());
var aPerson = new Person("Srinivas", "Gudipati");
console.log(aPerson.getFullName());
