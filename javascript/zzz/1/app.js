/*
	concat: Joins two or more arrays, and returns a copy of the joined arrays
*/

var concat1 = [1, 2, 3];
var concat2 = [4, 5, 6];
var concat3 = [7, 8, 9];
console.log(concat1.concat(concat2, concat3)); // 1, 2, 3, 4, 5, 6, 7, 8, 9
console.log(concat1); // 1, 2, 3

/*
	copyWithin: Copies array elements within the array, to and from specified positions
*/

var copyWithin1 = ['0', '1', '2', '3', '4', '5', '6'];
//console.log(copyWithin1.copyWithin(3)); // ['0', '1', '2', '0', '1', '2', '3']
//console.log(copyWithin1.copyWithin(-2)); // ['0', '1', '2', '3', '4', '0', '1']
//console.log(copyWithin1.copyWithin(7)); // ['0', '1', '2', '3', '4', '5', '6']
//console.log(copyWithin1.copyWithin(3, 2)); // ['0', '1', '2', '2', '3', '4', '5']
//console.log(copyWithin1.copyWithin(3, 2, 3)); // ['0', '1', '2', '2', '4', '5', '6']
console.log(copyWithin1.copyWithin(3, 2, -1)); // ['0', '1', '2', '2', '3', '4', '5']
console.log(copyWithin1); // ['0', '1', '2', '2', '3', '4', '5']

/*
	every: Checks if every element in an array pass a test
*/

var every1 = [
	{name: "Srinivas", age: 41},
	{name: "Arulmani", age: 39},
	{name: "Sudiksha", age: 11},
	{name: "Samiksha", age: 9},
	{name: "Sanjay", age: 4}
];
console.log(every1.every(function(person) {
	return person.age < 40;
}));
console.log(every1);

/*
	fill: Fill the elements in an array with a static value
*/

var fill1 = ['Yahoo', 'Altavista', 'Ask.com'];
console.log(fill1.fill('Google', 2)); // ['Yahoo', 'Altavista', 'Google']
console.log(fill1); // ['Yahoo', 'Altavista', 'Google']

/*
	filter: Creates a new array with every element in an array that pass a test
*/

var filter1 = [1, 3, 6, 9, 10];
console.log(filter1.filter(function(elem) {
	return elem%2 === 0;
})); // [6, 10]
console.log(filter1); // [1, 3, 6, 9, 10]


/*
	reduce: Applies function on accumulator and each value of the array to reduce it to a single value
*/

var reduce1 = [0, 1, 2, 3, 4];
console.log(reduce1.reduce(function(prevValue, currValue, currIndex, arr) {
	console.log(`prevalue = ${prevValue}`);
	console.log(`currValue = ${currValue}`);
	console.log(`currIndex = ${currIndex}`);
	console.log(`arr = ${arr}`);
	console.log(`return value = ${prevValue + currValue}`);
	return prevValue + currValue;
}, -10));
console.log(reduce1);

var reduce2 = [[1, 2], [3, 4], [5, 6]];
console.log(reduce2.reduce(function(prevValue, currValue) {
	return prevValue.concat(currValue);
}));