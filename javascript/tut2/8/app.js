console.log('"1" + undefined is ', "1" + undefined);
console.log('"1" + null is ', "1" + null);
console.log('"1" + false is ', "1" + false);
console.log('"1" + 2 is ', "1" + 2);
console.log('"1" + "2" is ', "1" + "2");
console.log('"1" + NaN is ', "1" + NaN);
console.log('"1" + {name: "Srinivas"} is ', "1" + {name: "Srinivas"});
console.log('"1" + function() {} is ', "1" + function() {});

console.log('------------------------------------------------------');

console.log('1 + undefined is ', 1 + undefined);
console.log('1 + null is ', 1 + null);
console.log('1 + false is ', 1 + false);
console.log('1 + 2 is ', 1 + 2);
console.log('1 + "2" is ', 1 + "2");
console.log('1 + NaN is ', 1 + NaN);
console.log('1 + {name: "Srinivas"} is ', 1 + {name: "Srinivas"});
console.log('1 + function() {} is ', 1 + function() {});

console.log('------------------------------------------------------');

console.log('true + undefined is ', true + undefined);
console.log('true + null is ', true + null);
console.log('true + false is ', true + false);
console.log('true + 2 is ', true + 2);
console.log('true + "2" is ', true + "2");
console.log('true + NaN is ', true + NaN);
console.log('true + {name: "Srinivas"} is ', true + {name: "Srinivas"});
console.log('true + function() {} is ', true + function() {});

console.log('------------------------------------------------------');

console.log('Number(NaN) is ', Number(NaN));
console.log('Number(undefined) is ', Number(undefined));
console.log('Number(null) is ', Number(null));
console.log('Number("") is ', Number(""));
console.log('Number("1") is ', Number("1"));
console.log('Number("abc") is ', Number("abc"));
console.log('Number(false) is ', Number(false));
console.log('Number(true) is ', Number(true));

console.log('------------------------------------------------------');

console.log('Boolean(NaN) is ', Boolean(NaN));
console.log('Boolean(undefined) is ', Boolean(undefined));
console.log('Boolean(null) is ', Boolean(null));
console.log('Boolean("") is ', Boolean(""));
console.log('Boolean(0) is ', Boolean(0));
console.log('Boolean("1") is ', Boolean("1"));
console.log('Boolean(1) is ', Boolean(1));
console.log('Boolean("abc") is ', Boolean("abc"));

console.log('------------------------------------------------------');

console.log('3 < 2 < 1 is', 3 < 2 < 1);

console.log('"2" < "12" is ', "2" < "12");
console.log('"2" > "12" is ', "2" > "12");
console.log("false == 0 is ", false == 0);
console.log("null == 0 is ", null == 0);
console.log("undefined == 0 is ", undefined == 0);
console.log('"" == 0 is ', "" == 0);
console.log('"" == false is ', "" == false);

console.log('"3" === 3 is ', "3" === 3);

var a;
a =0;

if (a || a === 0) {
	console.log('Something is there!');
}

console.log("true || false is ", true || false);

console.log('undefined || "hello" is ', undefined || "hello");

console.log('"hi" || "hello" is ', "hi" || "hello");

console.log('0 || 1 is ', 0 || 1);

console.log('0 || 0 is ', 0 || 0);

function hello(name) {
	name = name || "<your name here>";
	console.log("Hello ", name);
}

hello();
hello("");
hello("Srinivas!");