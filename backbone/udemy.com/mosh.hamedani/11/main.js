// Topic: Views: Handling DOM events

var Song = Backbone.Model.extend();

var SongView = Backbone.View.extend({
	events: {
		'click': 'onClick',
		'click .bookmark': 'onBookmarkClick'
	},
	onClick: function() {
		console.log("Listen Clicked");
	},
	onBookmarkClick: function(e) {
		e.stopPropagation();
		console.log("Bookmark Clicked");
	},
	render: function() {
		this.$el.html(this.model.get("title") + '<button>Listen</button>' + '<button class="bookmark">Bookmark</button>');
	}
});

var songView = new SongView({ el: '#container', model: new Song({ title: 'Song 1' }) });
songView.render();