define([
	'backbone',
	'models/boat'], function(Backbone, Boat) {
	
	var Boats = Backbone.Collection.extend({
		model: Boat
	});

	return Boats;
});