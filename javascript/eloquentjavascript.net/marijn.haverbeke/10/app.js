// Topic: arrays and objects

var obj = {
	flag: false,
	1: 1,
	"string 1": "string",
	o: {
		a: 1
	},
	foo: function() {
		console.log("foo");
	},
	arr: [true, 2, "another string", {b: 1}, function() {return "bar"}, [1, 2, 3]]
};

console.log(obj.flag);
console.log(obj["flag"]);
//console.log(obj.1); // since 1 is not a valid variable name this will not work
console.log(obj[1]);
console.log(obj["string 1"]);
console.log(obj.arr["0"]);
console.log(obj.arr[3]["b"]);
console.log(obj.arr[4]());
console.log(obj.arr[5][2]);
console.log(obj.arr[5]["length"]);