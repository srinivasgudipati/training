Object.create = null;

if(!Object.create) {
	Object.create = function(o) {
		function F() {}
		F.prototype = o;
		return new F();
	}
}


var person = {
	firstname: "Default",
	lastname: "Default",
	greet: function() {
		return 'Hi ' + this.firstname;
	}
};

var john = Object.create(person);
john.firstname = 'John';

console.log(john.greet());