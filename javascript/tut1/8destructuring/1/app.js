// Topic: Destructuring: Objects

// ES5
var expense = {
	type: 'Business',
	price: '$40 USD'
}

var type = expense.type;
var price = expense.price;

console.log(type);
console.log(price);

console.log('-----');

// ES6
var expense1 = {
	type1: 'Business',
	price1: '$40 USD'
}

const { type1, price1 } = expense1;

console.log(type1);
console.log(price1);